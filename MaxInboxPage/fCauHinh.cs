﻿using MCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxcare
{
    public partial class fCauHinh : Form
    {
        public fCauHinh()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            JSON_Settings settings = new JSON_Settings("CauHinhNhanTin");
            settings.Update("ckbNhanTinVanBan", ckbNhanTinVanBan.Checked);
            settings.Update("ckbSendAnh", ckbSendAnh.Checked);
            settings.Update("ckbInboxAll", ckbInboxAll.Checked);
            settings.Update("txtNoiDung", txtNoiDung.Text);
            settings.Update("txtAnh", txtAnh.Text);
            settings.Update("nudCountInviteFrom", nudCountInviteFrom.Value);
            settings.Update("nudCountInviteTo", nudCountInviteTo.Value);
            settings.Update("nudDelayFrom", nudDelayFrom.Value);
            settings.Update("nudSoLuongAnhFrom", nudSoLuongAnhFrom.Value);
            settings.Update("nudDelayTo", nudDelayTo.Value);
            settings.Update("nudSoLuongAnhTo", nudSoLuongAnhTo.Value);
            settings.Save();
            this.Close();
        }

        private void fCauHinh_Load(object sender, EventArgs e)
        {
            JSON_Settings settings = new JSON_Settings("CauHinhNhanTin");
            nudCountInviteFrom.Value = settings.GetValueInt("nudCountInvite", 10);
            nudDelayFrom.Value = settings.GetValueInt("nudDelayFrom", 2);
            nudDelayTo.Value = settings.GetValueInt("nudDelayTo", 5);
            ckbInboxAll.Checked = settings.GetValueBool("ckbInboxAll");
            nudCountInviteTo.Value = settings.GetValueInt("nudCountInviteTo");
            nudSoLuongAnhFrom.Value = settings.GetValueInt("nudSoLuongAnhFrom");
            nudSoLuongAnhTo.Value = settings.GetValueInt("nudSoLuongAnhTo");
            txtNoiDung.Text = settings.GetValue("txtNoiDung");
            txtAnh.Text = settings.GetValue("txtAnh");
            ckbNhanTinVanBan.Checked = settings.GetValueBool("ckbNhanTinVanBan");
            ckbSendAnh.Checked = settings.GetValueBool("ckbSendAnh");
        }

        private void ckbNhanTinVanBan_CheckedChanged(object sender, EventArgs e)
        {
            plComment.Enabled = ckbNhanTinVanBan.Checked;
        }

        private void ckbSendAnh_CheckedChanged(object sender, EventArgs e)
        {
            plAnh.Enabled = ckbSendAnh.Checked;

        }

        private void ckbInboxAll_CheckedChanged(object sender, EventArgs e)
        {
            nudCountInviteFrom.Enabled = !ckbInboxAll.Checked;
            nudCountInviteTo.Enabled = !ckbInboxAll.Checked;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void nudCountInviteTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void nudCountInviteFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
    }
}
