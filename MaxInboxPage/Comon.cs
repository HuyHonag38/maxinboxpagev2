﻿using DeviceId;
using HttpRequest;
using License.RNCryptor;
using maxregisterfacebook;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using xNet;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace maxcare
{
    public class Comon
    {
        public static int getWidthScreen;
        public static int getHeightScreen;

        public static string userName;
        public static string passWord;
        public static string valueReturnApiKey;

        


        /// <summary>
        /// Get token eaag using cookie facebook
        /// </summary>
        /// <param name="cookie">Cookie of facebook account</param>
        /// <returns>token eaag facebook</returns>
        public static string GetTokenBussinessFromCookie(string cookie)
        {
            string Token = "";
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
            });
            #endregion
            string GetDataToken = request.Request("GET", "https://business.facebook.com/business_locations/");
            Token = Regex.Match(GetDataToken, "EAAG(.*?)\"").Value.Replace("\"", "");
            return Token;

            //string data = "";
            //string uid = Regex.Match(cookie, "c_user=(.*?);").Groups[1].Value;
            //if (valueReturnApiKey.Equals("") || valueReturnApiKey.Contains("account\":false") || valueReturnApiKey.Contains("device\":false"))
            //{
            //    return "EaagABCDTEDDFDIT";
            //}
            //if (cookie != "" && cookie.Contains("c_user="))
            //{
            //    #region Khai báo request
            //    RequestHTTP request = new RequestHTTP();
            //    request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            //    request.SetKeepAlive(true);
            //    request.SetDefaultHeaders(new string[]
            //    {
            //        "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            //        "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36",
            //        "cookie: "+cookie
            //    });
            //    #endregion

            //    string GetDataToken = request.Request("GET", "https://business.facebook.com/select/?next=https%3A%2F%2Fbusiness.facebook.com%2Fhome").ToString();
            //    string BusinessID = Regex.Match(GetDataToken, "\"businessID\":\"(.*?)\"").Groups[1].Value;

            //    if (BusinessID != "")
            //    {
            //        GetDataToken = request.Request("GET", "https://business.facebook.com/home?business_id=" + BusinessID).ToString();
            //        string Token = Regex.Match(GetDataToken, "EAAG(.*?)\"").Value.Replace("\"", "");
            //        data = Token;
            //    }
            //    else
            //    {
            //        string getdata = request.Request("GET", "https://business.facebook.com/overview/").ToString();
            //        //string encrypted = Regex.Match(getdata, "encrypted\":\"(.*?)\"").Groups[1].Value;
            //        string htmlHome = request.Request("GET", "https://mbasic.facebook.com/");
            //        string fb_dtsg = Regex.Match(getdata, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
            //        //string jazoest = Regex.Match(getdata, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;

            //        string FullData = "__user=" + uid + "&__pc=PHASED:DEFAULT&__a=1&fb_dtsg=" + fb_dtsg;
            //        string GetHtml = request.Request("POST", "https://business.facebook.com/business/create_account/?brand_name=" + uid + "&first_name=Tung&last_name=Long&email=" + uid + "@gmail.com&timezone_id=140&business_category=OTHER&city=&country=US&state=a1&legal_name=&phone_number=&postal_code=&street1=&street2=&website_url=&is_b2b=false", null, Encoding.UTF8.GetBytes(FullData));
            //        File.WriteAllText("haha.html",GetHtml);
            //        GetDataToken = request.Request("GET", "https://business.facebook.com/select/?next=https%3A%2F%2Fbusiness.facebook.com%2Fhome");
            //        BusinessID = Regex.Match(GetDataToken, "\"businessID\":\"(.*?)\"").Groups[1].Value;

            //        GetDataToken = request.Request("GET", "https://business.facebook.com/home?business_id=" + BusinessID).ToString();
            //        string Token = Regex.Match(GetDataToken, "EAAG(.*?)\"").Value.Replace("\"", "");
            //        data = Token;
            //    }
            //}
            //return data;
        }

        public static void QuitChrome(ChromeDriver chrome)
        {
            try
            {
                chrome.Quit();
            }
            catch { }
        }
        public static ChromeDriver OpenChrome(ChromeDriver chrome, bool isHideCMD, bool isHideImage, string UserAgent, string LinkProfile, bool isHideChrome,
            Point Size, Point Position, string Proxy, int TimeForSearchingElement = 0, int TimeforLoadingPage = 0)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            if (isHideCMD)
            {
                service.HideCommandPromptWindow = true;
            }

            ChromeOptions options = new ChromeOptions();
            options.AddArguments(new string[] {
                "--disable-notifications",
                "--window-size="+Size.X+","+Size.Y,
                "--window-position="+Position.X+","+Position.Y,
                "--no-sandbox",
                "--disable-gpu",// applicable to windows os only
                "--disable-dev-shm-usage",//overcome limited resource problems       
                "--disable-web-security",
                "--disable-rtc-smoothness-algorithm",
                "--disable-webrtc-hw-decoding",
                "--disable-webrtc-hw-encoding",
                "--disable-webrtc-multiple-routes",
                "--disable-webrtc-hw-vp8-encoding",
                "--enforce-webrtc-ip-permission-check",
                "--force-webrtc-ip-handling-policy",
                "--ignore-certificate-errors",
                "--disable-infobars",
                "--mute-audio",
                "--disable-popup-blocking",
                "--user-agent="+UserAgent
            });

            options.AddUserProfilePreference("profile.default_content_setting_values.notifications", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.plugins", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.popups", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.geolocation", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.auto_select_certificate", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.mixed_script", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_mic", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_camera", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.protocol_handlers", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.midi_sysex", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.push_messaging", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.ssl_cert_decisions", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.metro_switch_to_desktop", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.protected_media_identifier", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.site_engagement", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.durable_storage", 1);
            options.AddUserProfilePreference("profile.managed_default_content_settings.images", 1);
            options.AddUserProfilePreference("useAutomationExtension", true);

            if (isHideChrome)
            {
                LinkProfile = "";
                options.AddArgument("--headless");
            }

            if (isHideImage)
                options.AddArgument("--blink-settings=imagesEnabled=false");

            if (!string.IsNullOrEmpty(LinkProfile.Trim()))
                options.AddArgument("--user-data-dir=" + LinkProfile);

            if (Proxy == "-1")
                Proxy = "";
            if (!string.IsNullOrEmpty(Proxy.Trim()))
                options.AddArgument("--proxy-server= 127.0.0.1:" + Proxy);

            chrome = new ChromeDriver(service, options);

            chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TimeForSearchingElement);
            chrome.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(TimeforLoadingPage);
            return chrome;
        }
        public static List<string> RemoveEmptyItems(List<string> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].Trim() == "")
                    lst.RemoveAt(i--);
            }
            return lst;
        }
        public static string CountNumberGroupUsingCookie(string cookie)
        {
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "cookie: "+cookie
                });
                #endregion
                string a = request.Request("GET", "https://mbasic.facebook.com/groups/?seemore&refid=27");
                MatchCollection match = Regex.Matches(a, "</li>");
                return match.Count.ToString();
            }
            catch
            {
                return "";
            }
        }
        public static string CountNumberFriendUsingCookie(string cookie)
        {
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "cookie: "+cookie
                });
                #endregion

                string a = request.Request("GET", "https://mbasic.facebook.com/profile.php?v=friends&refid=17");
                string match = Regex.Match(a, "allactivity\\?refid=17\"(.*?)</h3>").Value.ToString();
                string count= Regex.Match(match, @"\((.*?)\)").Groups[1].Value.ToString().Replace(".", "");
                if (count == "")
                    count = "0";
                return count;
            }
            catch
            {
                return "";
            }
            
        }
        public static string ConvertToShortCookie(string cookie)
        {
            try
            {
                string c1 = Regex.Match(cookie, "c_user=(.*?);").Value;
                string c2 = Regex.Match(cookie, "xs=(.*?);").Value;
                string c3 = Regex.Match(cookie, "fr=(.*?);").Value;
                string c4 = Regex.Match(cookie, "datr=(.*?);").Value;
                return c1 + c2 + c3 + c4;
            }
            catch
            {
                return cookie;
            }
        }

        /// <summary>
        /// typeSearch: 0-Id
        /// </summary>
        public static bool SearchElement(ChromeDriver chrome, int typeSearch, string value)
        {
            try
            {
                switch (typeSearch)
                {
                    case 0:
                        var x = chrome.FindElementById(value);
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public static bool CheckWall(string uid)
        {
            bool isLive = false;

            try
            {
                #region Khai báo request
                xNet.HttpRequest xRequest = new xNet.HttpRequest();
                xRequest.KeepAlive = true;
                xRequest.Cookies = new CookieDictionary();
                xRequest.AddHeader(HttpHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                xRequest.AddHeader(HttpHeader.AcceptLanguage, "en-US,en;q=0.5");
                xRequest.UserAgent = Http.ChromeUserAgent();
                #endregion

                string url = $"https://graph.facebook.com/{uid}/picture";
                string html = xRequest.Get(url).ToString();
                isLive = true;
            }
            catch
            {
            }

            return isLive;
        }
        
        public static bool CheckAvatar(string uid)
        {
            string status = "";
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string a = request.Request("GET", "https://graph.facebook.com/"+uid+"/picture");
            if (a.Contains("error"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static string CheckAccMailYahoo(string username, string pass)
        {
            string isValid = "";
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string resultInput = request.Request("GET", "https://login.yahoo.com/config/login");
            string acrumb = Regex.Match(resultInput, "acrumb\" value=\"(.*?)\"").Groups[1].Value;
            string sessionIndex = Regex.Match(resultInput, "sessionIndex\" value=\"(.*?)\"").Groups[1].Value;
            string persistent = Regex.Match(resultInput, "persistent\" value=\"(.*?)\"").Groups[1].Value;
            string dataPost = "acrumb=" + acrumb + "&sessionIndex=" + sessionIndex + "&username=" + username + "&passwd=&signin=Ti%E1%BA%BFp%C2%A0theo&persistent=" + persistent;
            resultInput = request.Request("POST", "https://login.yahoo.com/config/login", null, Encoding.ASCII.GetBytes(dataPost));
            string crumb = Regex.Match(resultInput, "crumb\" value=\"(.*?)\"").Groups[1].Value;
            string passwordContext = Regex.Match(resultInput, "passwordContext\" value=\"(.*?)\"").Groups[1].Value;
            dataPost = "browser-fp-data=%7B%22language%22%3A%22en-US%22%2C%22colorDepth%22%3A24%2C%22deviceMemory%22%3A8%2C%22pixelRatio%22%3A1%2C%22hardwareConcurrency%22%3A8%2C%22timezoneOffset%22%3A-420%2C%22timezone%22%3A%22Asia%2FBangkok%22%2C%22sessionStorage%22%3A1%2C%22localStorage%22%3A1%2C%22indexedDb%22%3A1%2C%22openDatabase%22%3A1%2C%22cpuClass%22%3A%22unknown%22%2C%22platform%22%3A%22Win32%22%2C%22doNotTrack%22%3A%221%22%2C%22plugins%22%3A%7B%22count%22%3A3%2C%22hash%22%3A%22e43a8bc708fc490225cde0663b28278c%22%7D%2C%22canvas%22%3A%22canvas+winding%3Ayes%7Ecanvas%22%2C%22webgl%22%3A1%2C%22webglVendorAndRenderer%22%3A%22Google+Inc.%7EANGLE+%28Intel%28R%29+UHD+Graphics+630+Direct3D11+vs_5_0+ps_5_0%29%22%2C%22adBlock%22%3A1%2C%22hasLiedLanguages%22%3A0%2C%22hasLiedResolution%22%3A0%2C%22hasLiedOs%22%3A0%2C%22hasLiedBrowser%22%3A0%2C%22touchSupport%22%3A%7B%22points%22%3A0%2C%22event%22%3A0%2C%22start%22%3A0%7D%2C%22fonts%22%3A%7B%22count%22%3A45%2C%22hash%22%3A%2246a30c0488455f08568f3e573502b25e%22%7D%2C%22audio%22%3A%22124.0434474653739%22%2C%22resolution%22%3A%7B%22w%22%3A%221920%22%2C%22h%22%3A%221080%22%7D%2C%22availableResolution%22%3A%7B%22w%22%3A%221040%22%2C%22h%22%3A%221920%22%7D%2C%22ts%22%3A%7B%22serve%22%3A1559177497471%2C%22render%22%3A1559177497688%7D%7D&crumb=" + crumb + "&acrumb=" + acrumb + "&sessionIndex=" + sessionIndex + "&displayName=phuonglazy&username=" + username + "&passwordContext=" + passwordContext + "&password=" + pass + "&verifyPassword=%C4%90%C4%83ng+nh%E1%BA%ADp";
            resultInput = request.Request("POST", "https://login.yahoo.com/account/challenge/password", null, Encoding.ASCII.GetBytes(dataPost));
            string htmlMail = request.Request("GET", "https://mail.yahoo.com").ToString();
            //File.WriteAllText("dmm.html", resultInput);
            if (resultInput.Contains("login-passwd") == false && resultInput.Contains(username.Replace("@yahoo.com", "")))
            {
                //valid
                isValid = "1|";
            }
            else
            {
                isValid = "0|";
            }
            string cookie = request.GetCookiesString("https://login.yahoo.com");
            return isValid + cookie;
        }

        public static string CheckAccMailYahooCookie(string username, string cookieMail)
        {
            string isValid = "";
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookieMail
            });
            #endregion
            string htmlMail = request.Request("GET", "https://mail.yahoo.com").ToString();
            //File.WriteAllText("dmm.html", resultInput);
            if (htmlMail.Contains("login-username") == false && htmlMail.Contains(username.Replace("@yahoo.com", "")))
            {
                //valid
                isValid = "1|";
            }
            else
            {
                isValid = "0|";
            }
            string cookie = request.GetCookiesString("https://login.yahoo.com");
            return isValid + cookie;
        }

        public static string GetOtpYahooFromCookie(string cook, int timeout)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cook
            });
            #endregion
            string htmlMail = request.Request("GET", "https://mail.yahoo.com").ToString();
            //File.WriteAllText("dmm.html", htmlMail);
            bool isFind = false; string resetPassOTP = "";
            int timeStart = Environment.TickCount;
            do
            {
                if (Environment.TickCount - timeStart > timeout * 1000)
                {
                    return "";
                }
                string htmlYahoo = request.Request("GET", "https://mail.yahoo.com").ToString();
                string itemRow = Regex.Match(htmlYahoo, @"message-subject(.*?)</span>", RegexOptions.Singleline).Value;
                resetPassOTP = Regex.Match(itemRow, @"\d{6}", RegexOptions.Singleline).Value;
                if (resetPassOTP != "")
                {
                    isFind = true;
                    //delete
                    break;
                }
            } while (isFind == false);
            return resetPassOTP;
        }


        public static void ExportError(ChromeDriver chrome, string error)
        {
            try
            {
                Random rrrd = new Random();
                string fileName = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" +DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + "_" + rrrd.Next(1, 1000);

                string html = "";
                if (chrome != null)
                {
                    html = chrome.ExecuteScript("var markup = document.documentElement.innerHTML;return markup;").ToString();
                    Screenshot image = ((ITakesScreenshot)chrome).GetScreenshot();
                    image.SaveAsFile(@"log\images\" + fileName + ".png");
                    File.WriteAllText(@"log\html\" + fileName + ".html", html);
                }

                File.AppendAllText(@"log\log.txt", DateTime.Now + "|<" + fileName + ">|" + error + Environment.NewLine);
            }
            catch { }
        }


        public static bool AddFriendAcceptFriendUidFromCookie(string cookie, string uid)
        {
            bool isAdd = false;
            try
            {
                if (CheckLiveCookie(cookie).Equals("Die"))
                    return isAdd;
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
                });
                #endregion
                string html = request.Request("GET", "https://mbasic.facebook.com/" + uid);
                string linkAddFriend = Regex.Match(html, "/a/mobile/friends/profile_add_friend(.*?)\"").Value.Replace("\"", "").Replace("amp;", "");
                if (linkAddFriend.Equals(""))
                    return isAdd;
                linkAddFriend = "https://mbasic.facebook.com" + linkAddFriend;
                html = request.Request("GET", linkAddFriend);
                Thread.Sleep(300);
                html = request.Request("GET", "https://mbasic.facebook.com/" + uid);
                if (html.Contains("profile_add_friend"))
                    isAdd = false;
                else
                    isAdd = true;
            }
            catch { }

            //if accept link contain confirm

            return isAdd;
        }

        /// <summary>
        /// Invite like page
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="uid"></param>
        /// <returns>invitesucced</returns>
        public static string InviteLikePageCookie(string cookie, string idPage, int timeSleep = 0)
        {
            int countSuccess = 0;
            try
            {
                if (CheckLiveCookie(cookie).Equals("Die"))
                    return "";
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
                });
                #endregion
                string linkRq = "https://mbasic.facebook.com/send_page_invite/?pageid=" + idPage;
                bool isFindMore = false;
                do
                {
                    isFindMore = false;
                    string html = request.Request("GET", linkRq);
                    MatchCollection linkInvites = Regex.Matches(html, "/pages/friend_invite/send/(.*?)\"");
                    for (int i = 0; i < linkInvites.Count; i++)
                    {
                        string linkInvite = "https://mbasic.facebook.com" + linkInvites[i].Value.Replace("\"", "").Replace("amp;", "");

                        request.Request("GET", linkInvite);
                        countSuccess++;
                        if (timeSleep != 0)
                            Thread.Sleep(timeSleep);
                    }

                    if (html.Contains("offset"))
                    {
                        isFindMore = true;
                        continue;
                    }
                } while (isFindMore == true);

            }
            catch { }

            //if accept link contain confirm

            return "" + countSuccess;
        }

        public static string CheckLiveCookie(string cookie)
        {
            cookie = ConvertToStandardCookie(cookie);
            string data = "Die";
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
                });
                #endregion

                string uid = Regex.Match(cookie, "c_user=(.*?);").Groups[1].Value;
                string html = request.Request("GET", "https://www.facebook.com/me");

                if (uid.Equals("") == false && html.Contains(uid) && html.Contains("entity_id") && html.Contains("checkpointSubmitButton") == false)
                {
                    data = "Live";
                }
            }
            catch
            {
            }
            return data;
        }

        public static string GetFbdtsg(string cookie, string uA = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36")
        {
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: application/x-www-form-urlencoded",
                    "user-agent: "+uA,
                    "cookie: "+cookie
                });
                #endregion
                string rq = request.Request("GET", "https://m.facebook.com/ajax/dtsg/?__ajax__=true");
                rq = rq.Replace("for (;;);", "");
                JObject jo = JObject.Parse(rq);
                return jo["payload"]["token"].ToString();
            }
            catch
            {
                return "";
            }
        }
        #region Share
        /// <summary>
        /// Nếu share lên tường cá nhân thì id_group để trống
        /// </summary>
        public static string AutoShare(string cookie, string content, string link, string id_group)
        {
            string idpost = GetIdPost(link);
            if (idpost == "")
                return "";

            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "cookie: "+cookie
            });
            #endregion

            string myId = Regex.Match(cookie, "c_user=(.*?);").Groups[1].Value;
            string fb_dtsg = GetFbdtsg(cookie);
            string html = request.Request("GET", "https://m.facebook.com/me");
            string jazoest = Regex.Match(html, "name=\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
            string privacyx =  Regex.Match(html, "name=\"privacyx\" value=\"(.*?)\"").Groups[1].Value;
            string __rev = Regex.Match(html, "__spin_r\":(.*?),").Groups[1].Value;
            string __spin_r = __rev;
            string __spin_t = Regex.Match(html, "__spin_t\":(.*?),").Groups[1].Value;

            string Data = "__user=" + myId + "&__a=1&__dyn=7AgNe-UOByEogDxyHqzGomzFEbEyGzEy4aheC267Uqzob4q2i5UK3u2C3-u5RyUoxGEbbyEjKewXwgUOdwJyFElwzxCuifz8nxm1Dxa2m4o6e2e79oeGwaWum1NwJxCq7ooxu6U8kU4m3mbx-2K1KUkBzXG6o6CEWu4EhwG-U99m4-3Cfz8-4U-5898Gfxm7omyUnG12AgG4eeKi8wg8jyE5WcyES48y8xK3yeCzEmgK7o88vwEy8iwSwjU4W2WE9EjwtUym2mfxW68lBwcO&__csr=&__req=13&__pc=PHASED%3ADEFAULT&dpr=1&__rev=" + __rev + "&__s=k2qltm%3Act038n%3Av4okyh&__hsi=6766055500147629515-0&fb_dtsg=" + WebUtility.UrlEncode(fb_dtsg) + "&jazoest=" + jazoest + "&__spin_r=" + __spin_r + "&__spin_b=trunk&__spin_t=" + __spin_t;
            string link_post = "";
            if (id_group != "")
                link_post = "https://www.facebook.com/share/dialog/submit/?app_id=2309869772&audience_type=group&audience_targets[0]=" + id_group + "&composer_session_id=38700f24-cbee-4aaa-a626-9fd6f4056e3a&ephemeral_ttl_mode=0&ft[tn]=J]-R-R&ft[type]=25&ft[mf_story_key]=&ft[top_level_post_id]=&ft[tl_objid]=&ft[content_owner_id_new]=&ft[throwback_story_fbid]=&ft[story_location]=9&ft[story_attachment_style]=share&ft[fbfeed_location]=5&internalextra[feedback_source]=2&is_forced_reshare_of_post=true&message=" + WebUtility.UrlEncode(content) + "&owner_id=&post_id=" + idpost + "&share_to_group_as_page=false&share_type=99&shared_ad_id=&source=osbach&is_throwback_post=false&url=&shared_from_post_id=&logging_session_id=e24e7b30-b545-4305-a233-b9874afe63d4&perform_messenger_logging=true&video_start_time_ms=0&is_app_content_token=false&av=" + myId;
            else
                link_post = "https://www.facebook.com/share/dialog/submit/?app_id=2309869772&audience_type=self&composer_session_id=38700f24-cbee-4aaa-a626-9fd6f4056e3a&ephemeral_ttl_mode=0&ft[tn]=J]-R-R&ft[type]=25&ft[mf_story_key]=&ft[top_level_post_id]=&ft[tl_objid]=&ft[content_owner_id_new]=&ft[throwback_story_fbid]=&ft[story_location]=9&ft[story_attachment_style]=share&ft[fbfeed_location]=5&internalextra[feedback_source]=2&is_forced_reshare_of_post=true&message=" + WebUtility.UrlEncode(content) + "&owner_id=&post_id=" + idpost + "&privacy=" + privacyx + "&share_to_group_as_page=false&share_type=99&shared_ad_id=&source=osbach&is_throwback_post=false&url=&shared_from_post_id=&logging_session_id=e24e7b30-b545-4305-a233-b9874afe63d4&perform_messenger_logging=true&video_start_time_ms=0&is_app_content_token=false&av=" + myId;
            string respond = request.Request("POST", link_post, null, Encoding.ASCII.GetBytes(Data));
            string id_post = Regex.Match(respond, "object_id\":(.*?),").Groups[1].Value;
            return id_post;
        }
        public static string GetIdPost(string Link)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                "content-type: application/x-www-form-urlencoded",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
            });
            #endregion
            string html = request.Request("GET", Link);
            //string owner_id = Regex.Match(html, "entity_id\":\"(.*?)\"", RegexOptions.Singleline).Groups[1].Value;
            string id = Regex.Match(html, @"\\""post_fbid\\"":(.*?)}", RegexOptions.Singleline).Groups[1].Value;
            if (id == "")
                id = Regex.Match(html, "share_fbid:\"(.*?)\"", RegexOptions.Singleline).Groups[1].Value;
            if (id == "")
                id = Regex.Match(html, "videos/(.*?)/", RegexOptions.Singleline).Groups[1].Value;

            return id;
        }
        #endregion
        public static void PublicPost(string cookie)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "cookie: "+cookie
            });
            #endregion

            string html = request.Request("GET", "https://mbasic.facebook.com/privacy/touch/composer/selector/?logging_source=composer_options");

            string link = Regex.Match(html, "/privacy/save(.*?)\"").Value.Replace("\"", "").Replace("&amp;", "&");
            string respond = request.Request("GET", "https://mbasic.facebook.com" + link);
        }

        public static List<string> GetListFriendsFromCookie(string cookie)
        {
            List<string> listFriends = new List<string>();
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
            });
            #endregion
            int page = 0;
            string linkReadFriends = "https://mbasic.facebook.com/friends/center/friends/?ppk=" + page;
            bool isMore = true;
            try
            {
                while (isMore)
                {
                    string html = request.Request("GET", linkReadFriends);
                    MatchCollection matchFriend = Regex.Matches(html, "/friends/hovercard(.*?)<");
                    for (int i = 0; i < matchFriend.Count; i++)
                    {
                        string uidFr = Regex.Match(matchFriend[i].Value, "uid=(.*?)&").Groups[1].Value;
                        string nameFr = Regex.Match(matchFriend[i].Value, ">(.*?)<").Groups[1].Value;
                        listFriends.Add(uidFr + "|" + nameFr);
                    }
                    //next link
                    linkReadFriends = Regex.Match(html, "/friends/center/friends/.ppk=(.*?)\"").Value.Replace("\"", "").Replace("amp;", "");
                    if (linkReadFriends.Equals(""))
                        isMore = false;
                    else
                    {
                        isMore = true;
                        linkReadFriends = "https://mbasic.facebook.com" + linkReadFriends;
                    }
                }
            }
            catch { }
            return listFriends;
        }

        public static bool PublicListFriends(string cookie)
        {
            bool isPublic = false;
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "cookie: "+cookie
                });
                #endregion
                string html = request.Request("GET", "https://mbasic.facebook.com/me/friends");
                string linkChangePrivacy = Regex.Match(html, "/privacyx/selector/(.*?)\"").Value.Replace("\"", "").Replace("amp;", "");
                linkChangePrivacy = "https://mbasic.facebook.com" + linkChangePrivacy;
                html = request.Request("GET", linkChangePrivacy);
                string linkPublic = Regex.Match(html, "/a/privacy/.px=300645083384735(.*?)\"").Value.Replace("\"", "").Replace("amp;", "");
                linkPublic = "https://mbasic.facebook.com" + linkPublic;
                html = request.Request("GET", linkPublic);
                isPublic = true;
            }
            catch
            {
                isPublic = false;
            }
            return isPublic;
        }


        /// <summary>
        /// Get list page is Admin
        /// </summary>
        /// <param name="token"></param>
        /// <returns>list page string id|like </returns>
        public static List<string> CheckMyPages(string token)
        {
            List<string> lp = new List<string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion
                string html = request.Request("GET", "https://graph.facebook.com/me/accounts?access_token=" + token + "&fields=id,likes&limit=50");
                JObject objPage = JObject.Parse(html);
                for (int i = 0; i < objPage["data"].Count(); i++)
                {
                    lp.Add(objPage["data"][i]["id"].ToString() + "|" + objPage["data"][i]["likes"].ToString());
                }
            }
            catch
            {

            }

            return lp;
        }
        /// <summary>
        /// Get all my BM
        /// </summary>
        /// <param name="token"></param>
        /// <returns>list BM</returns>
        public static List<string> CheckMyBM(string token)
        {
            List<string> lp = new List<string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion
                string html = request.Request("GET", "https://graph.facebook.com/me/businesses?access_token=" + token + "&fields=id,name&limit=50");
                JObject objPage = JObject.Parse(html);
                for (int i = 0; i < objPage["data"].Count(); i++)
                {
                    lp.Add(objPage["data"][i]["id"].ToString() + "|" + objPage["data"][i]["name"].ToString());
                }
            }
            catch
            {

            }

            return lp;
        }

        public static List<string> CheckMyTkqc(string token)
        {
            List<string> lp = new List<string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion
                string html = request.Request("GET", "https://graph.facebook.com/me/adaccounts?access_token=" + token + "&fields=id,min_billing_threshold&limit=100");
                JObject objPage = JObject.Parse(html);
                for (int i = 0; i < objPage["data"].Count(); i++)
                {
                    try
                    {
                        lp.Add(objPage["data"][i]["id"].ToString() + "|" + objPage["data"][i]["min_billing_threshold"]["amount"].ToString() + " " + objPage["data"][i]["min_billing_threshold"]["currency"].ToString());
                    }
                    catch { }
                }
            }
            catch
            {

            }

            return lp;
        }
        /// <summary>
        /// Check gr is Admin
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static List<string> CheckMyGroupAdmin(string token)
        {
            List<string> lp = new List<string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion
                string html = request.Request("GET", "https://graph.facebook.com/me/groups?access_token=" + token + "&fields=id,administrator,member_count&limit=5000");
                JObject objPage = JObject.Parse(html);
                for (int i = 0; i < objPage["data"].Count(); i++)
                {
                    try
                    {
                        if (objPage["data"][i]["administrator"].ToString().Equals("True"))
                            lp.Add(objPage["data"][i]["id"].ToString() + "|" + objPage["data"][i]["member_count"].ToString());
                    }
                    catch { }
                }
            }
            catch
            {

            }

            return lp;
        }


        public static Dictionary<string, string> GetInforUserFromUid(string token, string uid, string cookie="")
        {
            Dictionary<string, string> dicData = new Dictionary<string, string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion

                int dem = 0;
                xxx:
                string infor = request.Request("GET", "https://graph.facebook.com/v2.11/" + uid + "?fields=id,name,email,gender,birthday,friends.limit(0),groups.limit(5000){id}&access_token=" + token);
                JObject objUser = JObject.Parse(infor);
                string email = "", groups = "", friends = "", phone = "", gender = "", birthday = "";
                string info = "Live";
                string name = objUser["name"].ToString();
                try
                {
                    birthday = objUser["birthday"].ToString();
                }
                catch { }
                try
                {
                    gender = objUser["gender"].ToString();
                }
                catch { }
                try
                {
                    email = objUser["email"].ToString();
                }
                catch { }
                try
                {
                    phone = objUser["mobile_phone"].ToString();
                }
                catch { }
                try
                {
                    friends = objUser["friends"]["summary"]["total_count"].ToString();
                }
                catch {
                    if (cookie != "" && Comon.CheckLiveCookie(cookie) == "Live" && dem == 0)
                    {
                        if (PublicListFriends(cookie))
                        {
                            dem++;
                            goto xxx;
                        }
                    }
                }
                if (friends == "")
                    friends = "0";
                try
                {
                    groups = "" + objUser["groups"]["data"].Count();
                }
                catch
                {

                }
                if (groups == "")
                    groups = "0";
                dicData.Add("uid", objUser["id"].ToString());
                dicData.Add("name", objUser["name"].ToString());
                dicData.Add("birthday", birthday);
                dicData.Add("gender", gender);
                dicData.Add("token", token);
                dicData.Add("email", email);
                dicData.Add("phone", phone);
                dicData.Add("friends", friends);
                dicData.Add("groups", groups);
                dicData.Add("info", info);
            }
            catch
            {
                dicData.Add("info", "Die");
            }
            return dicData;
        }

        public static List<string> GetRandomFriendFromToken(string token, int limit)
        {
            List<string> listtt = new List<string>();
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string infor = request.Request("GET", "https://api.facebook.com/method/fql.query?query=select%20uid2%20from%20friend%20where%20uid1%20=%20me()%20order%20by%20rand()%20limit%20" + limit + "&access_token=" + token + "&format=json").ToString();
            JArray jsonArray = JArray.Parse(infor);
            var objUser = jsonArray.OfType<JObject>().ToList();
            for (int i = 0; i < objUser.Count(); i++)
            {
                listtt.Add(objUser[i]["uid2"].ToString());
            }
            return listtt;
        }

        public static Dictionary<string, string> GetInforUserFromToken(string token)
        {
            Dictionary<string, string> dicData = new Dictionary<string, string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                });
                #endregion
                //string infor = request.Request("GET","https://graph.facebook.com/v2.11/me?fields=id,name,email,mobile_phone,gender,birthday,friends,groups.limit(5000){id}&access_token=" + token).ToString();
                string infor = request.Request("GET", "https://graph.facebook.com/v2.11/me?fields=id,name,email,gender,birthday,friends.limit(0),groups.limit(5000){id}&access_token=" + token).ToString();
                JObject objUser = JObject.Parse(infor);
                string email = "", groups = "", friends = "", phone = "", gender = "";
                string info = "Live";
                string name = objUser["name"].ToString();
                string birthday = "";
                try
                {
                    birthday = objUser["birthday"].ToString();
                }
                catch
                {
                }
                 
                try
                {
                    gender = objUser["gender"].ToString();
                }
                catch { }
                try
                {
                    email = objUser["email"].ToString();
                }
                catch { }
                try
                {
                    phone = objUser["mobile_phone"].ToString();
                }
                catch { }
                friends = objUser["friends"]["summary"]["total_count"].ToString();
                try
                {
                    groups = "" + objUser["groups"]["data"].Count();
                }
                catch
                {}
                if (groups == "")
                    groups = "0";

                dicData.Add("uid", objUser["id"].ToString());
                dicData.Add("name", objUser["name"].ToString());
                dicData.Add("birthday", objUser["birthday"].ToString());
                dicData.Add("gender", gender);
                dicData.Add("token", token);
                dicData.Add("email", email);
                dicData.Add("phone", phone);
                dicData.Add("friends", friends);
                dicData.Add("groups", groups);
                dicData.Add("info", info);
            }
            catch (Exception ex)
            {
                dicData.Add("info", "Die");
            }
            return dicData;
        }

        public static string GetTokenAndroidFromUidPass(string username, string password)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string returnString = "";
            bool isError = false;
            string apiKey = "882a8490361da98702bf97a021ddc14d";
            string sig = Encryptor("api_key=" + apiKey + "email=" + username + "format=JSONlocale=vi_vnmethod=auth.loginpassword=" + password + "return_ssl_resources=0v=1.062f8ce9f74b12f84c123cc23437a4a32").ToLower();
            string getAcc = request.Request("GET", "https://api.facebook.com/restserver.php?&api_key=" + apiKey + "&email=" + username + "&format=JSON&locale=vi_vn&method=auth.login&password=" + password + "&return_ssl_resources=0&v=1.0&sig=" + sig);
            JObject objAcc = JObject.Parse(getAcc);
            try
            {
                returnString = objAcc["access_token"].ToString();
            }
            catch
            {
                isError = true;
                returnString = "" + objAcc["error_code"];
            }
            return isError ? "1|" + returnString : "0|" + returnString;
        }

        public static string GetTokenIosFromUidPass(string username, string password)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string returnString = "";
            bool isError = false;
            string apiKey = "3e7c78e35a76a9299309885393b02d97";
            string sig = Encryptor("api_key=" + apiKey + "email=" + username + "format=JSONlocale=vi_vnmethod=auth.loginpassword=" + password + "return_ssl_resources=0v=1.0c1e620fa708a1d5696fb991c1bde5662").ToLower();
            string getAcc = request.Request("GET", "https://api.facebook.com/restserver.php?&api_key=" + apiKey + "&email=" + username + "&format=JSON&locale=vi_vn&method=auth.login&password=" + password + "&return_ssl_resources=0&v=1.0&sig=" + sig);
            JObject objAcc = JObject.Parse(getAcc);
            try
            {
                returnString = objAcc["access_token"].ToString();
            }
            catch
            {
                isError = true;
                returnString = "" + objAcc["error_code"];
            }
            return isError ? "1|" + returnString : "0|" + returnString;
        }

        public static string CheckAccountUidPass(string username, string password)
        {
            //1 live 2 cp 3 sai pass
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string stringReturn = "";
            string getdata = request.Request("GET", "https://mbasic.facebook.com");
            string fb_dtsg = Regex.Match(getdata, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
            string jazoest = Regex.Match(getdata, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
            string m_ts = Regex.Match(getdata, "\"m_ts\" value=\"(.*?)\"").Groups[1].Value;
            string li = Regex.Match(getdata, "\"li\" value=\"(.*?)\"").Groups[1].Value;
            string[] arrHeader = new string[]
            {
                "referer: https://mbasic.facebook.com/checkpoint/?_rdr"
            };
            string htmlLogin = request.Request("POST", "https://mbasic.facebook.com/login/device-based/regular/login/?refsrc=https%3A%2F%2Fmbasic.facebook.com%2F&lwv=100&refid=8", null, Encoding.UTF8.GetBytes("fb_dtsg=" + fb_dtsg + "%3D&jazoest=" + jazoest + "&m_ts=" + m_ts + "&li=" + li + "&try_number=0&unrecognized_tries=0&email=" + username + "&pass=" + password + "&login=Log+In"));
            htmlLogin = request.Request("GET", "https://www.facebook.com/checkpoint/?next=https%3A%2F%2Fwww.facebook.com%2F");
            //File.WriteAllText("dmm.html", htmlLogin);
            if (htmlLogin.Contains("checkpoint/?next"))
            {
                stringReturn = "2|";
                //checkpoint
                fb_dtsg = Regex.Match(htmlLogin, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
                jazoest = Regex.Match(htmlLogin, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
                string nh = Regex.Match(htmlLogin, "\"nh\" value=\"(.*?)\"").Groups[1].Value;
                string client_rev = Regex.Match(htmlLogin, "client_revision\":(.*?),").Groups[1].Value;
                getdata = request.Request("POST", "https://www.facebook.com/checkpoint/async?next=https%3A%2F%2Fwww.facebook.com%2F", null, Encoding.UTF8.GetBytes("jazoest=" + jazoest + "&fb_dtsg=" + fb_dtsg + "&nh=" + nh + "&submit[Continue]=Ti%E1%BA%BFp%20t%E1%BB%A5c&__user=0&__a=1&__dyn=&__req=&__be=0&__pc=PHASED%3ADEFAULT&dpr=&__rev=" + client_rev));
                getdata = request.Request("GET", "https://www.facebook.com/checkpoint/?next=https%3A%2F%2Fwww.facebook.com%2F");
                //File.WriteAllText("dmm.html", getdata);
                MatchCollection verification_methods = Regex.Matches(getdata, "verification_method\" value=\"(.*?)\"");
                if (verification_methods.Count > 0)
                {
                    foreach (Match item in verification_methods)
                    {
                        stringReturn += item.Groups[1].Value + "-";
                    }
                    stringReturn = stringReturn.TrimEnd('-');
                }
                else
                {
                    if (getdata.Contains("full-name") || getdata.Contains("captcha"))
                    {
                        stringReturn += "72h";
                    }
                    else if (getdata.Contains("mvm uiP fsm"))
                    {
                        //Vo hieu hoa tai khoan vv
                        stringReturn += "vhh";
                    }

                }
                stringReturn += "|" + request.GetCookiesString("https://www.facebook.com/");
            }
            else if (htmlLogin.Contains("id=\"email\"") || htmlLogin.Contains("id=\"pass\""))
            {
                //changed pass
                stringReturn = "3|";
            }
            else
            {
                //live and return cookie
                stringReturn = "1|" + request.GetCookiesString("https://www.facebook.com/");
            }
            return stringReturn;
        }


        public static string SwapCheckpoint(string username, string password, string userAgent, string ip, int port)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: "+userAgent,
            });
            #endregion
            request.SetProxy(ip, port);
            request.usProxy = true;
            string stringReturn = "";
            string getdata = request.Request("GET", "https://mbasic.facebook.com");
            string fb_dtsg = Regex.Match(getdata, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
            string jazoest = Regex.Match(getdata, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
            string m_ts = Regex.Match(getdata, "\"m_ts\" value=\"(.*?)\"").Groups[1].Value;
            string li = Regex.Match(getdata, "\"li\" value=\"(.*?)\"").Groups[1].Value;
            string htmlLogin = request.Request("POST", "https://mbasic.facebook.com/login/device-based/regular/login/?refsrc=https%3A%2F%2Fmbasic.facebook.com%2F&lwv=100&refid=8", null, Encoding.UTF8.GetBytes("fb_dtsg=" + fb_dtsg + "%3D&jazoest=" + jazoest + "&m_ts=" + m_ts + "&li=" + li + "&try_number=0&unrecognized_tries=0&email=" + username + "&pass=" + password + "&login=Log+In"));
            htmlLogin = request.Request("GET", "https://m.facebook.com/checkpoint/?next=https%3A%2F%2Fm.facebook.com%2F");
            htmlLogin = request.Request("GET", "https://mbasic.facebook.com/checkpoint/?next=https%3A%2F%2Fmbasic.facebook.com%2F");
            htmlLogin = request.Request("GET", "https://www.facebook.com/checkpoint/?next=https%3A%2F%www.facebook.com%2F");
            return stringReturn;
        }

        public static string GetCheckpointFromIdMethod(string idMethod)
        {
            string stt = "";
            switch (idMethod)
            {
                case "3":
                    stt = "Ảnh";
                    break;
                case "2":
                    stt = "Ngày sinh";
                    break;
                case "20":
                    stt = "Tin nhắn";
                    break;
                case "4":
                    stt = "Otp";
                    break;
                case "34":
                    stt = "Otp";
                    break;
                case "14":
                    stt = "Thiết bị";
                    break;
                case "26":
                    stt = "Bạn bè";
                    break;
                case "18":
                    stt = "Bình luận";
                    break;
                case "72h":
                    stt = "72h";
                    break;
                case "vhh":
                    stt = "Vô hiệu hóa";
                    break;
                default:
                    break;
            }
            return stt;
        }

        public static List<string> RequestLoginUidPassMbasic(string username, string password)
        {
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            List<string> listReturn = new List<string>();
            string getdata = request.Request("GET", "https://mbasic.facebook.com");
            string fb_dtsg = Regex.Match(getdata, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
            string jazoest = Regex.Match(getdata, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
            string m_ts = Regex.Match(getdata, "\"m_ts\" value=\"(.*?)\"").Groups[1].Value;
            string li = Regex.Match(getdata, "\"li\" value=\"(.*?)\"").Groups[1].Value;
            string[] arrHeader = new string[]
            {
                "referer: https://mbasic.facebook.com/checkpoint/?_rdr"
            };
            string htmlLogin = request.Request("POST", "https://mbasic.facebook.com/login/device-based/regular/login/?refsrc=https%3A%2F%2Fmbasic.facebook.com%2F&lwv=100&refid=8", null, Encoding.UTF8.GetBytes("fb_dtsg=" + fb_dtsg + "%3D&jazoest=" + jazoest + "&m_ts=" + m_ts + "&li=" + li + "&try_number=0&unrecognized_tries=0&email=" + username + "&pass=" + password + "&login=Log+In"));
            //File.WriteAllText("dmm.html", htmlLogin);
            if (htmlLogin.Contains("checkpoint_title"))
            {
                //checkpoint
                fb_dtsg = Regex.Match(htmlLogin, "\"fb_dtsg\" value=\"(.*?)\"").Groups[1].Value;
                jazoest = Regex.Match(htmlLogin, "\"jazoest\" value=\"(.*?)\"").Groups[1].Value;
                string nh = Regex.Match(getdata, "\"nh\" value=\"(.*?)\"").Groups[1].Value;
                //getdata = request.Request("POST", "https://mbasic.facebook.com/login/checkpoint/", arrHeader, Encoding.UTF8.GetBytes("fb_dtsg=" + fb_dtsg + "%3D&jazoest=" + jazoest + "&checkpoint_data=&submit%5BContinue%5D=Continue&nh=" + nh));
                getdata = request.Request("GET", "https://www.facebook.com/checkpoint/?next=https%3A%2F%2Fwww.facebook.com%2F");
                string verification_method = Regex.Match(getdata, "<select name=\"verification_method(.*?)</select>").Value;
                //File.WriteAllText("dmm.html", getdata);
                MatchCollection matchCp = Regex.Matches(verification_method, @"\d{2}");
                foreach (Match item in matchCp)
                {
                    listReturn.Add(item.Value);
                }
            }
            else
            {
                //File.WriteAllText("dmm.html", htmlLogin);
            }
            return listReturn;
        }

        public static bool CheckMailYahoo(string mail)
        {
            bool isLive = false;
            mail = mail.Replace("@yahoo.com", "");
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                    "X-Requested-With: XMLHttpRequest",
                    "Cookie: AS=v=1&s=jc8Jz1UA",
                    "Referrer: https://login.yahoo.com/account/module/create?validateField=yid"
            });
            #endregion
            string checkLive = request.Request("POST", "https://login.yahoo.com/account/module/create?validateField=yid", null, Encoding.UTF8.GetBytes("browser-fp-data=&specId=yidReg&crumb=&acrumb=jc8Jz1UA&c=&s=&done=https%3A%2F%2Fwww.yahoo.com&googleIdToken=&authCode=&tos0=yahoo_freereg%7Cvn%7Cvi-VN&tos1=yahoo_comms_atos%7Cvn%7Cvi-VN&firstName=&lastName=&yid=" + mail + "&password=&shortCountryCode=VN&phone=&mm=&dd=&yyyy=&freeformGender="));
            if (checkLive.Contains("yid\",\"error"))
            {
                isLive = true;
            }
            return isLive;
        }

        public static bool CheckMailHotmail(string mail)
        {
            bool isLive = false;
            mail = mail.Replace("@yahoo.com", "");
            #region Khai báo request
            RequestHTTP request = new RequestHTTP();
            request.SetSSL(System.Net.SecurityProtocolType.Tls12);
            request.SetKeepAlive(true);
            request.SetDefaultHeaders(new string[]
            {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            });
            #endregion
            string checkLive = request.Request("GET", "https://login.microsoftonline.com/common/userrealm/?user=" + mail + "&api-version=2.1&stsRequest=rQIIAbPSySgpKSi20tcvyC8qSczRy81MLsovzk8ryc_LycxL1UvOz9XLL0rPTAGxioS4BC6HNYTFGB7zWy90MI6xcUHgKkZlwkboX2BkfMHIeItJ0L8o3TMlvNgtNSW1KLEkMz_vERNvaHFqkX9eTmVIfnZq3iRmvpz89My8-OKitPi0nPxyoADQhILE5JL4kszk7NSSXcwqiSaWqYZpKWm6xpZplromhiZmupYWiWa6FhYmZqaGFqlpFomJF1gEfrAwLmIFurmyae30LeVybvNE3C_Vi-5_e4pVPy_FOcvSIiIgNUPbJyU1KjE0PTcsMdezzCM5PN2oJN8zyN0g0MjZ3bzSuNzW0srwACcjAA2&checkForMicrosoftAccount=true").ToString();
            JObject oCheck = JObject.Parse(checkLive);
            if (oCheck["MicrosoftAccount"].ToString().Equals("1"))
            {
                isLive = false;
            }
            else
            {
                isLive = true;
            }
            return isLive;
        }

        public static bool ChangePryvacyMail(string cookie)
        {
            bool isDone = true;
            try
            {
                string uid = Regex.Match(cookie, "c_user=(.*?);").Groups[1].Value;
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36",
                    "cookie: "+cookie
                });
                #endregion
                string html = request.Request("GET", "https://www.facebook.com/");
                if (uid.Equals("") == false && html.Contains(uid) && html.Contains("name=\"fb_dtsg\" value="))
                {
                    string htmlDelete = request.Request("GET", "https://mbasic.facebook.com/settings/email/");
                    string linkPryvacy = Regex.Match(htmlDelete, "/privacyx/selector(.*?)\"").Value.Replace("amp;", "").Replace("\"", "");
                    string htmlPryvacy = request.Request("GET", "https://mbasic.facebook.com" + linkPryvacy + "&priv_expand=see_all");
                    string linkOnlyMe = Regex.Match(htmlPryvacy, "/a/privacy/.px=286958161406148(.*?)\"").Value.Replace("amp;", "").Replace("\"", "");
                    if (linkOnlyMe.Equals(""))
                    {
                        return false;
                    }
                    string doneRequest = request.Request("GET", "https://mbasic.facebook.com" + linkOnlyMe);
                    isDone = true;
                    //File.WriteAllText("dmm.html", doneRequest);
                }
                else
                {
                    isDone = false;
                }
            }
            catch
            {
                isDone = false;
            }
            return isDone;
        }

        public static string LoadErrorFromErrorcode(string temp)
        {
            string message = "";
            if (temp.Split('|')[0] == "1")
            {
                int error_code = Convert.ToInt32(temp.Split('|')[1]);
                switch (error_code)
                {
                    case 401:
                        message = "Changed pass";
                        break;
                    case 405:
                        message = "Checkpoint";
                        break;
                    case 400:
                        message = "Tài khoản không tồn tại";
                        break;
                    case 613:
                        message = "Giới hạn lấy token";
                        break;
                    case 104:
                        // incorrect signature
                        message = "Lỗi phần mềm";
                        break;
                    default:
                        message = "Lỗi hệ thống";
                        break;
                }
            }
            return message;
        }

        public static string Encryptor(string str)
        {
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(str);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static void resetDcom(string profileName)
        {
            Process process = new Process();
            process.StartInfo.FileName = "rasdial.exe";
            process.StartInfo.Arguments = "\"" + profileName + "\" /disconnect";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();
            Thread.Sleep(1000);
        }
        public static void DelayTime(double second)
        {
            Application.DoEvents();
            Thread.Sleep(Convert.ToInt32(second * 1000));
        }
        public static void startDcom(string profileName)
        {
            Thread.Sleep(1000);
            Process process = new Process();
            process.StartInfo.FileName = "rasdial.exe";
            process.StartInfo.Arguments = "\"" + profileName + "\"";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();
            Thread.Sleep(1500);
        }

        public static bool ChangeIP(int typeChangeIP, string profileDcom, int iTypeHotspot, string sLinkNord)
        {
            try
            {
                if (typeChangeIP == 0)
                {
                    return true;
                }
                else if (typeChangeIP == 1)
                {
                    //Change ip hma
                    Auto.ClickControlNoMouseImageFind("HMA VPN", "Chrome_RenderWidgetHostHWND", "Chrome Legacy Window", "images//change-hma.PNG");
                    Thread.Sleep(8000);
                    int timeStart = Environment.TickCount;
                    do
                    {
                        if (Environment.TickCount - timeStart > 20000)
                            return false;
                        Thread.Sleep(1000);
                    } while (Auto.ImageFind("HMA VPN", "images//wait-hma.PNG"));
                    return true;
                }
                else if (typeChangeIP == 2)
                {
                    resetDcom(profileDcom);
                    Thread.Sleep(500);
                    startDcom(profileDcom);
                    return true;
                }
                else if (typeChangeIP == 4)
                {
                    if (Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//disconnect-express.PNG"))
                        Thread.Sleep(5000);
                    if (Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//connect-express.PNG"))
                        Thread.Sleep(10000);
                    else
                    {
                        Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//connect-express.PNG");
                        Thread.Sleep(10000);
                    }

                    return true;
                }
                else if (typeChangeIP == 5)
                {
                    if (iTypeHotspot == 0)
                    {
                        //hostpot
                        int timeStart = Environment.TickCount;
                        if (Auto.ClickControlImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG"))
                            do
                            {
                                if (Environment.TickCount - timeStart > 20000)
                                    return false;
                                Thread.Sleep(1000);
                            } while (Auto.ImageFind("Hotspot Shield", "images//connect-hotspot.PNG") == false);
                        Auto.ClickControlImageFind("Hotspot Shield", "images//connect-hotspot.PNG");
                        timeStart = Environment.TickCount;
                        do
                        {
                            if (Environment.TickCount - timeStart > 20000)
                                return false;
                            Thread.Sleep(1000);
                        } while (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") == false);
                    }
                    else if (iTypeHotspot == 1)
                    {
                        Random rd = new Random();
                        //hostpot
                        int timeStart = Environment.TickCount;
                        string pointChange = Auto.ImageFind1("Hotspot Shield", "images//change-country-hotspot.PNG");
                        //WriteError(pointChange + "|" + DateTime.Now.ToString());
                        Point pointcc = new Point();
                        if (pointChange != "")
                            pointcc = new Point(Convert.ToInt32(pointChange.Split('|')[0]), Convert.ToInt32(pointChange.Split('|')[1]));
                        if (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") && Auto.ClickControlImageFind("Hotspot Shield", "images//change-country-hotspot.PNG", 0, 0, 20) && pointChange != null)
                        {
                            Thread.Sleep(2000);
                            KAutoHelper.AutoControl.MouseScroll(pointcc, rd.Next(-10, 10), true);
                            Thread.Sleep(1000);
                            KAutoHelper.AutoControl.MouseClick(pointcc.X, pointcc.Y);
                            timeStart = Environment.TickCount;
                            do
                            {
                                if (Environment.TickCount - timeStart > 20000)
                                    return false;
                                Thread.Sleep(1000);
                            } while (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") == false);
                        }
                    }
                }
                else if (typeChangeIP == 6)
                {
                    //nordvpn
                    Random rd = new Random();
                    string[] arrGroupName = new string[]
                    {
                    "Albania",
                    "Argentina",
                    "Australia",
                    "Austria",
                    "Belgium",
                    "Bosnia and Herzegovina",
                    "Brazil",
                    "Bulgaria",
                    "Canada",
                    "Chile",
                    "Costa Rica",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Georgia",
                    "Germany",
                    "Greece",
                    "Hong Kong",
                    "Hungary",
                    "Iceland",
                    "India",
                    "Indonesia",
                    "Ireland",
                    "Israel",
                    "Italy",
                    "Japan",
                    "Latvia",
                    "Luxembourg",
                    "Mexico",
                    "Moldova",
                    "Netherlands",
                    "New Zealand",
                    "North Macedonia",
                    "Norway",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Serbia",
                    "Singapore",
                    "Slovakia",
                    "Slovenia",
                    "South Africa",
                    "South Korea",
                    "Spain",
                    "Sweden",
                    "Switzedland",
                    "Taiwan",
                    "Thailand",
                    "Turkey",
                    "Ukraine",
                    "United Kingdom",
                    "United States",
                    "Vietnam"
                    };
                    RunCMD("\"" + sLinkNord + "\\NordVPN.exe\" -d");
                    Thread.Sleep(10000);
                    RunCMD("\"" + sLinkNord + "\\NordVPN.exe\" -c -g" + arrGroupName[rd.Next(0, arrGroupName.Length - 1)]);
                    int timeStart = Environment.TickCount;
                    Thread.Sleep(5000);
                    do
                    {
                        Thread.Sleep(1000);
                        if (Environment.TickCount - timeStart > 20000)
                            return false;
                    } while (Auto.ImageFind("", "images//disconnect-nord.PNG", "HwndWrapper[hsscp.exe;;34304629-92de-4140-a008-a2b4910674c9]") == false);
                }
                return true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex.ToString());
                return true;
            }
        }

        private static string RunCMD(string cmd)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            string output = cmdProcess.StandardOutput.ReadToEnd();
            cmdProcess.WaitForExit();
            if (String.IsNullOrEmpty(output))
                return "";
            return output;
        }

        public static void KillProcessChromeDriver()
        {
            Process[] chromeDriverProcesses = Process.GetProcessesByName("chromedriver");

            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
        }

        public static Point GetPointFromIndexPosition(int indexPos, int maxApp = 6)
        {
            Point location = new Point();
            int widthWindowChrome = (2 * getWidthScreen) / maxApp;
            int totalAppPerLine = maxApp / 2;
            while (indexPos > 5)
            {
                indexPos -= 6;
            }
            if (indexPos <= totalAppPerLine - 1)
            {
                location.Y = 0;
            }
            else if (indexPos < maxApp)
            {
                location.Y = getHeightScreen / 2;
                indexPos -= totalAppPerLine;
            }
            location.X = (indexPos) * (widthWindowChrome);
            return location;
        }

        private static string PrivateKey(string s)
        {
            return s + "minsoftware.tk";
        }
        public static string CreateRandomPassword(int length = 15)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length - 2; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            chars[13] = '1'; chars[14] = 'T';
            return new string(chars);
        }

        public static bool HasMethod(object objectToCheck, string methodName)
        {
            var type = objectToCheck.GetType();
            return type.GetMethod(methodName) != null;
        }

        public static string Decode_UTF8(string s)
        {
            byte[] bytes = Encoding.Default.GetBytes(s);
            return Encoding.UTF8.GetString(bytes);
        }

        public static double CompareTwoString(string str1, string str2)
        {
            double point = 0;
            string[] arr = str2.Split(' ');
            for (int i = 0; i < arr.Length; i++)
            {
                if (str2 != "" && str1.Contains(" " + arr[i] + " "))
                    point++;
            }
            return point / arr.Length;
        }

        public static string CheckSsh(string ssh)
        {
            int po = BitviseHandle.GetPortAvailable();
            try
            {
                return (BitviseHandle.Connect(ssh.Split('|')[0], ssh.Split('|')[1], ssh.Split('|')[2], po) == true ? "1|" + po : "0|" + po);
            }
            catch
            {
                return "0|" + po;
            }
        }
        private static string MaHoa(string x)
        {
            //Tạo MD5 
            MD5 mh = MD5.Create();
            //Chuyển kiểu chuổi thành kiểu byte
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(x);
            //mã hóa chuỗi đã chuyển
            byte[] hash = mh.ComputeHash(inputBytes);
            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X"));
            }
            return sb.ToString();
        }

        public static string GetIdKey(string a = "")
        {
            //License.Hardware hardware = new License.Hardware();
            //return hardware.getHDD();
            string deviceId = new DeviceIdBuilder()
            .AddMachineName()
            .AddProcessorId()
            .AddMotherboardSerialNumber()
            .AddSystemDriveSerialNumber()
            .ToString();
            return MaHoa(deviceId);
        }
        public static string GetAccessKey(string a = "")
        {
            if (a.Equals(""))
                a = GetIdKey();
            return EncodeMD5(PrivateKey(a));
        }

        public static string EncodeMD5(string txt)
        {
            String str = "";
            Byte[] buffer = System.Text.Encoding.UTF8.GetBytes(txt);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            buffer = md5.ComputeHash(buffer);
            foreach (Byte b in buffer)
            {
                str += b.ToString("X2");
            }
            return str.ToLower();
        }

        public static string GetUUID()
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = "CMD.exe",
                Arguments = "/C wmic csproduct get UUID"
            };
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
            process.WaitForExit();
            string text = process.StandardOutput.ReadToEnd();
            text = text.Replace("UUID", "").Replace("\n", "").Replace(" ", "").Replace("-", "").Replace("\r", "");
            return text;
        }

        public static string GetIdCpu()
        {
            string cpuInfo = string.Empty;
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();

            foreach (ManagementObject mo in moc)
            {
                cpuInfo = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return cpuInfo;
        }

        public static void Shutdown()
        {
            ManagementBaseObject mboShutdown = null;
            ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();

            // You can't shutdown without security privileges
            mcWin32.Scope.Options.EnablePrivileges = true;
            ManagementBaseObject mboShutdownParams =
                     mcWin32.GetMethodParameters("Win32Shutdown");

            // Flag 1 means we want to shut down the system. Use "2" to reboot.
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown",
                                               mboShutdownParams, null);
            }
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


        //kiem tra dinh dang mail
        public static bool IsValidMail(string emailaddress)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string ConvertToStandardCookie(string cookie)
        {
            try
            {
                string c1 = Regex.Match(cookie, "c_user=(.*?);").Value;
                string c2 = Regex.Match(cookie, "xs=(.*?);").Value;
                string c3 = Regex.Match(cookie, "fr=(.*?);").Value;
                string c4 = Regex.Match(cookie, "datr=(.*?);").Value;
                return c1 + c2 + c3 + c4;
            }
            catch
            {
                return cookie;
            }
        }

        public static void DisconnectSshPort(int port)
        {
            try
            {
                BitviseHandle.Disconnect(port);
            }
            catch { }
        }

        public static List<string> GetGroupFromCookie(string cookie, string uA = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36")
        {
            cookie = ConvertToStandardCookie(cookie);
            List<string> listGroup = new List<string>();
            try
            {
                #region Khai báo request
                RequestHTTP request = new RequestHTTP();
                request.SetSSL(System.Net.SecurityProtocolType.Tls12);
                request.SetKeepAlive(true);
                request.SetDefaultHeaders(new string[]
                {
                    "content-type: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "user-agent: "+uA,
                    "cookie: "+cookie
                });
                #endregion

                string html = request.Request("GET", "https://mbasic.facebook.com/groups/?seemore");
                MatchCollection linkGr = Regex.Matches(html, "<a href=\"/groups/[0-9]+\\?refid=27");
                for (int i = 0; i < linkGr.Count; i++)
                {
                    try
                    {
                        string idgr = Regex.Match(linkGr[i].Value, "groups/(.*?)\\?refid=27").Groups[1].Value.ToString();
                        if (idgr != "")
                            listGroup.Add(idgr);
                    }
                    catch { }
                }
            }
            catch
            {
            }

            return listGroup;
        }

        private static string[] VietNamChar = {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };

        public static string ToUnicode(string str)
        {
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }
    }
}