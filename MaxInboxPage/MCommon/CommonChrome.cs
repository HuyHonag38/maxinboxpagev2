﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MCommon
{
    public class CommonChrome
    {
        public static bool LoginFacebookUsingCookie(Chrome chrome, string cookie, string URL = "https://www.facebook.com")
        {
            bool isSuccess = false;
            try
            {
                if (!chrome.GetURL().StartsWith(URL))
                    chrome.GotoURL(URL);

                chrome.AddCookieIntoChrome(cookie);
                chrome.Refresh();
                isSuccess = CheckLiveCookie(chrome);
            }
            catch
            {
            }
            return isSuccess;
        }
        public static bool LoginFacebookUsingUidPass(Chrome chrome, string uid, string pass, string fa2 = "", string URL = "https://www.facebook.com")
        {
            bool isSuccess = false;
            try
            {
                int typeWeb = 0;//1-www, 2-m, 0-ko co
                if (chrome.GetURL().StartsWith("https://www.facebook"))
                    typeWeb = 1;
                else if (chrome.GetURL().StartsWith("https://m.facebook"))
                    typeWeb = 2;

                if (typeWeb == 0)
                {
                    chrome.GotoURL(URL);

                    if (chrome.GetURL().StartsWith("https://www.facebook"))
                        typeWeb = 1;
                    else if (chrome.GetURL().StartsWith("https://m.facebook"))
                        typeWeb = 2;
                }

                if (typeWeb == 1)
                {
                    if (!chrome.GetURL().Contains("https://www.facebook.com/login"))
                        chrome.GotoURL("https://www.facebook.com/login");

                    chrome.Click(1, "email");
                    chrome.SendKeys(1, "email", uid, 0.1);
                    chrome.DelayTime(1);

                    chrome.Click(1, "pass");
                    chrome.SendKeys(1, "pass", pass, 0.1);
                    chrome.DelayTime(1);

                    chrome.Click(1, "loginbutton");
                    chrome.DelayTime(1);

                    if (chrome.CheckExistElement("#approvals_code", 5) && fa2 != "")
                    {
                        string input = Common.GetTotp(fa2.Replace(" ", "").Replace("\n", ""));

                        if (input != "")
                        {
                            chrome.SendKeys(1, "approvals_code", input, 0.1);
                            chrome.DelayTime(1);

                            chrome.Click(1, "checkpointSubmitButton");
                            chrome.DelayTime(1);

                            if (!chrome.CheckExistElement("#approvals_code"))
                            {
                                chrome.Click(1, "checkpointSubmitButton");
                                chrome.DelayTime(1);

                                int stt = 0;
                                while (chrome.CheckExistElement("#checkpointSubmitButton", 3))
                                {
                                    if (chrome.CheckExistElement("#captcha_response") || stt == 7)
                                        break;
                                    chrome.Click(1, "checkpointSubmitButton");
                                    chrome.DelayTime(1);
                                    stt++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!chrome.GetURL().Contains("https://m.facebook.com/login"))
                        chrome.GotoURL("https://m.facebook.com/login");

                    chrome.SendKeys(1, "m_login_email", uid, 0.1);
                    chrome.DelayTime(1);

                    chrome.SendKeys(1, "m_login_password", pass, 0.1);
                    chrome.DelayTime(1);

                    chrome.Click(2, "login");
                    chrome.DelayTime(1);

                    if (chrome.CheckExistElement("#approvals_code", 5) && fa2 != "")
                    {
                        string input = Common.GetTotp(fa2.Replace(" ", "").Replace("\n", ""));

                        if (input != "")
                        {
                            chrome.SendKeys(1, "approvals_code", input, 0.1);
                            chrome.DelayTime(1);

                            chrome.Click(1, "checkpointSubmitButton-actual-button");
                            chrome.DelayTime(1);

                            if (!chrome.CheckExistElement("#approvals_code"))
                            {
                                chrome.Click(1, "checkpointSubmitButton-actual-button");
                                chrome.DelayTime(1);

                                int stt = 0;
                                while (chrome.CheckExistElement("#checkpointSubmitButton-actual-button", 3))
                                {
                                    if (chrome.CheckExistElement("#captcha_response") || stt == 7)
                                        break;
                                    chrome.Click(1, "checkpointSubmitButton-actual-button");
                                    chrome.DelayTime(1);
                                    stt++;
                                }
                            }
                        }
                    }
                }
                chrome.DelayTime(1);
                
                isSuccess = CheckLiveCookie(chrome);
            }
            catch(Exception ex)
            {
                MCommon.Common.ExportError(chrome, ex, "Login Uid Pass Fail");
            }
            return isSuccess;
        }

        public static string GetTokenEAAAAZ(Chrome chrome)
        {
            string token = "";
            try
            {
                string rq = RequestGet(chrome, "https://m.facebook.com/composer/ocelot/async_loader/?publisher=feed", "https://m.facebook.com");
                token = Regex.Match(rq, "EAAAAZ(.*?)\"").Value.Replace("\"", "").Replace(@"\", "");
            }
            catch
            {
            }
            return token;
        }
        public static string GetTokenEAAAAZrequest(string cookie)
        {
            string token = "";
            try
            {
                RequestXNet request = new RequestXNet(cookie);
                string rq = request.RequestGet("https://m.facebook.com/composer/ocelot/async_loader/?publisher=feed");
                token = Regex.Match(rq, "EAAAAZ(.*?)\"").Value.Replace("\"", "").Replace(@"\", "");
            }
            catch
            {

            }
            return token;
        }
        public static bool CheckLiveCookie(Chrome chrome, string URL = "https://www.facebook.com")
        {
            bool isLive = false;
            try
            {
                int typeWeb = 0;//1-www, 2-m, 0-ko co
                if (chrome.GetURL().StartsWith("https://www.facebook"))
                    typeWeb = 1;
                else if (chrome.GetURL().StartsWith("https://m.facebook"))
                    typeWeb = 2;

                if (typeWeb == 0)
                {
                    chrome.GotoURL(URL);

                    if (chrome.GetURL().StartsWith("https://www.facebook"))
                        typeWeb = 1;
                    else if (chrome.GetURL().StartsWith("https://m.facebook"))
                        typeWeb = 2;
                }

                string body = "";
                if (typeWeb == 1)
                    body = (string)chrome.ExecuteScript("async function CheckLiveCookie() { var output = '0|0'; try { var response = await fetch('https://www.facebook.com/me'); if (response.ok) { var body = await response.text(); if (body.includes('id=\"code_in_cliff\"')||body.includes('name=\"new\"')||body.includes('name=\"c\"')) output = '1|0'; else if (!body.includes('checkpointSubmitButton') && !body.includes('checkpointBottomBar') && !body.includes('captcha_response') && body.match(new RegExp('\"USER_ID\":\"(.*?)\"'))[1] == (document.cookie + ';').match(new RegExp('c_user=(.*?);'))[1]) output = '1|1'; } } catch {} return output; }; var c = await CheckLiveCookie(); return c");
                else
                    body = (string)chrome.ExecuteScript("async function CheckLiveCookie() { var output = '0|0'; try { var response = await fetch('https://m.facebook.com/me'); if (response.ok) { var body = await response.text(); if (body.includes('id=\"code_in_cliff\"')||body.includes('name=\"new\"')||body.includes('name=\"c\"')) output = '1|0'; if (!body.includes('checkpointSubmitButton') && !body.includes('checkpointBottomBar') && !body.includes('captcha_response') && body.match(new RegExp('\"USER_ID\":\"(.*?)\"'))[1] == (document.cookie+';').match(new RegExp('c_user=(.*?);'))[1]) output = '1|1'; } } catch {} return output; }; var c = await CheckLiveCookie(); return c;");

                if (body.Split('|')[0] == "1")
                    isLive = true;
            }
            catch
            {
            }
            return isLive;
        }
        public static bool IsCheckpoint(Chrome chrome)
        {
            return chrome.CheckExistElement("#checkpointSubmitButton") || chrome.CheckExistElement("#captcha_response") || chrome.CheckExistElement("#checkpointBottomBar");
        }
        public static string GetTokenEAAG(Chrome chrome)
        {
            string token = "";
            try
            {
                if (!chrome.GetURL().Contains("https://business.facebook.com/"))
                    chrome.GotoURL("https://business.facebook.com/");
                token = (string)chrome.ExecuteScript("async function GetTokenEaag() { var output = ''; try { var response = await fetch('https://business.facebook.com/business_locations/'); if (response.ok) { var body = await response.text(); output=body.match(new RegExp('EAAG(.*?)\"'))[0].replace('\"',''); } } catch {} return output; }; var c = await GetTokenEaag(); return c;");
            }
            catch
            {
            }
            return token;
        }

        public static string RequestGet(Chrome chrome, string url, string website)
        {
            try
            {
                if (!chrome.GetURL().StartsWith(website))
                    chrome.GotoURL(website);
                string rq = (string)chrome.ExecuteScript("async function RequestGet() { var output = ''; try { var response = await fetch('" + url + "'); if (response.ok) { var body = await response.text(); return body; } } catch {} return output; }; var c = await RequestGet(); return c;");
                return rq;
            }
            catch { }

            return "";
        }
        public static string RequestPost(Chrome chrome, string url, string data, string website)
        {
            try
            {
                if (!chrome.GetURL().StartsWith(website))
                    chrome.GotoURL(website);
                string rq = (string)chrome.ExecuteScript("async function RequestPost() { var output = ''; try { var response = await fetch('" + url + "', { method: 'POST', body: '" + data + "', headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }); if (response.ok) { var body = await response.text(); return body; } } catch {} return output; }; var c = await RequestPost(); return c;");
                return rq;
            }
            catch { }

            return "";
        }
    }
}
