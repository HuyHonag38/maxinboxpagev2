﻿using maxcare;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCommon
{
    class JSON_Settings
    {
        private string PathFileSetting;
        JObject json;

        public JSON_Settings(string NameFileSetting, string uid = "")
        {
            if (uid == "")
            {
                this.PathFileSetting = Base.currentPath + "settings\\" + NameFileSetting + ".json";
            }
            else
            {
                if (!Directory.Exists(Base.currentPath + "configspost\\post\\" + uid))
                {
                    Directory.CreateDirectory(Base.currentPath + "configspost\\post\\" + uid);
                }
                this.PathFileSetting = Base.currentPath + "configspost\\post\\" + uid + "\\" + NameFileSetting + ".json";
            }

            if (!File.Exists(PathFileSetting))
                using (StreamWriter w = File.AppendText(PathFileSetting)) { }
            try
            {
                json = JObject.Parse(File.ReadAllText(PathFileSetting));
            }
            catch
            {
                json = new JObject();
            }
        }
        //public JSON_Settingss(string NameFileSetting,string id_post)
        //{
        //    this.PathFileSetting = Base.currentPath + @"\\settings\\" + NameFileSetting + ".json";
        //    if (!File.Exists(PathFileSetting))
        //        using (StreamWriter w = File.AppendText(PathFileSetting)) { }
        //    try
        //    {
        //        json = JObject.Parse(File.ReadAllText(PathFileSetting));
        //    }
        //    catch
        //    {
        //        json = new JObject();
        //    }
        //}
        public string GetValue(string key, string valueDefault = "")
        {
            string output = valueDefault;
            try
            {
                output = json[key] == null ? valueDefault : json[key].ToString();
            }
            catch
            { }
            return output;
        }
        public List<string> GetValueList(string key)
        {
            List<string> output = new List<string>();
            try
            {
                output = GetValue(key).Split('\n').ToList();
                output = MCommon.Common.RemoveEmptyItems(output);
            }
            catch
            { }
            return output;
        }
        public int GetValueInt(string key, int valueDefault = 0)
        {
            int output = valueDefault;
            try
            {
                output = json[key] == null ? valueDefault : Convert.ToInt32(json[key].ToString());
            }
            catch
            { }
            return output;
        }
        public bool GetValueBool(string key, bool valueDefault = false)
        {
            bool output = valueDefault;
            try
            {
                output = json[key] == null ? valueDefault : Convert.ToBoolean(json[key].ToString());
            }
            catch
            { }
            return output;
        }
        public void Add(string key, string value)
        {
            try
            {
                if (!json.ContainsKey(key))
                    json.Add(key, value);
                else
                    json[key] = value;
            }
            catch (Exception ex)
            { }
        }
        public void Update(string key, List<string> lst)
        {
            try
            {
                json[key] = string.Join("\n", lst).ToString();
            }
            catch
            { }
        }
        public void Update(string key, object value)
        {
            try
            {
                json[key] = value.ToString();
            }
            catch
            { }
        }
        public void Remove(string key)
        {
            try
            {
                json.Remove(key);
            }
            catch
            { }
        }
        public void Save()
        {
            try
            {
                File.WriteAllText(PathFileSetting, json.ToString());
            }
            catch
            {
            }
        }
        public string GetFullString()
        {
            string output = "";
            try
            {
                output = File.ReadAllText(PathFileSetting);
            }
            catch (Exception ex)
            {
            }
            return output;
        }
    }
}
