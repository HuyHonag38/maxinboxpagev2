﻿using KAutoHelper;
using maxregisterfacebook;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCommon
{
    public class Common
    {
        public List<string> CloneList(List<string> lstFrom)
        {
            List<string> lstOutput = new List<string>();
            try
            {
                for (int i = 0; i < lstFrom.Count; i++)
                {
                    lstOutput.Add(lstFrom[i]);
                }
            }
            catch
            {

            }
            return lstOutput;
        }
        public static string SpinText(string text, Random rand)
        {
            int i, j, e = -1;
            char[] curls = new char[] { '{', '}' };
            text += '~';
            string[] parts;
            do
            {
                i = e;
                e = -1;
                while ((i = text.IndexOf('{', i + 1)) != -1)
                {
                    j = i;
                    while ((j = text.IndexOfAny(curls, j + 1)) != -1 && text[j] != '}')
                    {
                        if (e == -1) e = i;
                        i = j;
                    }
                    if (j != -1)
                    {
                        parts = text.Substring(i + 1, (j - 1) - (i + 1 - 1)).Split('|');
                        text = text.Remove(i, j - (i - 1)).Insert(i, parts[rand.Next(parts.Length)]);
                    }
                }
            }
            while (e-- != -1);

            return text.Remove(text.Length - 1);
        }
        public static void CreateFile(string pathFile)
        {
            try
            {
                if (!File.Exists(pathFile))
                    File.AppendAllText(pathFile, "");
            }
            catch
            {
            }
        }
        public static void CreateFolder(string pathFolder)
        {
            try
            {
                if (!Directory.Exists(pathFolder))
                    Directory.CreateDirectory(pathFolder);
            }
            catch
            {
            }
        }

        public static void ShowForm(Form f)
        {
            f.ShowInTaskbar = false;
            f.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="type">1-success, 2-error, 3-warning</param>
        public static void ShowMessageBox(object s, int type)
        {
            switch (type)
            {
                case 1:
                    MessageBox.Show(s.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    break;
                case 2:
                    MessageBox.Show(s.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
                case 3:
                    MessageBox.Show(s.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Remove empty item of List
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> RemoveEmptyItems(List<string> lst)
        {
            List<string> lstOutput = new List<string>();
            string item = "";
            for (int i = 0; i < lst.Count; i++)
            {
                item = lst[i].Trim();
                if (item != "")
                    lstOutput.Add(item);
            }
            return lstOutput;
        }

        /// <summary>
        /// Auto change ip Dcom
        /// </summary>
        /// <param name="profileDcom"></param>
        public static void ResetDcom(string profileDcom)
        {
            Process process = new Process();
            process.StartInfo.FileName = "rasdial.exe";
            process.StartInfo.Arguments = "\"" + profileDcom + "\" /disconnect";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();

            Thread.Sleep(3000);
            process = new Process();
            process.StartInfo.FileName = "rasdial.exe";
            process.StartInfo.Arguments = "\"" + profileDcom + "\"";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();
            Thread.Sleep(1500);
        }
        public static string TrimEnd(string text, string value)
        {
            if (!text.EndsWith(value))
                return text;

            return text.Remove(text.LastIndexOf(value));
        }
        public static void SaveDatagridview(DataGridView dgv, string FilePath, char splitChar = '|')
        {
            List<string> list = new List<string>();
            string row = "";
            object r = null;
            for (int j = 0; j < dgv.RowCount; j++)
            {
                row = "";
                for (int i = 0; i < dgv.ColumnCount; i++)
                {
                    r = dgv.Rows[j].Cells[i].Value;
                    row += r == null ? splitChar.ToString() : (r + splitChar.ToString());
                }
                row = row.TrimEnd(splitChar);
                list.Add(row);
            }
            File.WriteAllLines(FilePath, list);
        }

        public static void LoadDatagridview(DataGridView dgv, string namePath, char splitChar = '|')
        {
            if (!File.Exists(namePath))
                MCommon.Common.CreateFile(namePath);
            List<string> list = File.ReadAllLines(namePath).ToList();
            string row = "";
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    row = list[i];
                    dgv.Rows.Add(row.Split(splitChar));
                }
            }
        }
        public static string SelectFolder()
        {
            string path = "";
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        path = fbd.SelectedPath;
                    }
                }
            }
            catch { }
            return path;
        }

        public static void SetStatusDataGridView(DataGridView dgv, int row, int col, object status)
        {
            try
            {
                Application.DoEvents();
                dgv.Invoke(new MethodInvoker(delegate ()
                {
                    dgv.Rows[row].Cells[col].Value = status;
                }));
            }
            catch { }
        }
        public static void SetStatusDataGridView(DataGridView dgv, int row, string colName, object status)
        {
            try
            {
                Application.DoEvents();
                dgv.Invoke(new MethodInvoker(delegate ()
                {
                    dgv.Rows[row].Cells[colName].Value = status;
                }));
            }
            catch { }
        }

        public static string GetStatusDataGridView(DataGridView dgv, int row, int col)
        {
            string output = "";

            dgv.Invoke(new MethodInvoker(delegate ()
            {
                try
                {
                    output = dgv.Rows[row].Cells[col].Value.ToString();
                }
                catch { }
            }));

            return output;
        }
        public static string GetStatusDataGridView(DataGridView dgv, int row, string colName)
        {
            string output = "";

            dgv.Invoke(new MethodInvoker(delegate ()
            {
                try
                {
                    output = dgv.Rows[row].Cells[colName].Value.ToString();
                }
                catch(Exception ex) {}
            }));

            return output;
        }

        public static void KillProcess(string nameProcess)
        {
            try
            {
                foreach (Process proc in Process.GetProcessesByName(nameProcess))
                {
                    proc.Kill();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Check string is contain latinh char?
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool CheckBasicString(string text)
        {
            bool result = true;
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '.'))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        /// <summary>
        /// Remove char is not latin in string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveCharNotLatin(string text)
        {
            string result = "";
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    result += c;
                }
            }
            return result;
        }

        /// <summary>
        /// Convert Text to UTF-8
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ConvertToUTF8(string text)
        {
            byte[] bytes = Encoding.Default.GetBytes(text);
            text = Encoding.UTF8.GetString(bytes);
            return text;
        }
        public static bool IsNumber(string pValue)
        {
            if (pValue == "")
                return false;
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public static bool IsContainNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (Char.IsDigit(c))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Open browser with html text
        /// </summary>
        /// <param name="text"></param>
        public static void ReadHtmlText(string html)
        {
            string path = "zzz999.html";
            File.WriteAllText(path, html);
            Process.Start(path);
        }
        /// <summary>
        /// Download string from Url
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public static string ReadHTMLCode(string Url)
        {
            try
            {
                WebClient webClient = new WebClient();
                byte[] reqHTML = webClient.DownloadData(Url);
                UTF8Encoding objUTF8 = new UTF8Encoding();
                return objUTF8.GetString(reqHTML);
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Check is a address mail?
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        public static bool IsValidMail(string emailaddress)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public static string Md5Encode(string text)
        {
            MD5 obj = MD5.Create();
            byte[] data = obj.ComputeHash(System.Text.Encoding.UTF8.GetBytes(text));
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                s.Append(data[i].ToString("X2"));
            }
            return s.ToString();
        }
        public static string Base64Encode(string base64Decoded)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(base64Decoded);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64Encoded)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64Encoded);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string CreateRandomString(int lengText, Random rd = null)
        {
            string outPut = "";
            if (rd == null)
                rd = new Random();
            string validChars = "abcdefghijklmnopqrstuvwxyz";
            for (int i = 0; i < lengText; i++)
            {
                outPut += validChars[rd.Next(0, validChars.Length)];
            }
            return outPut;
        }
        public static string CreateRandomNumber(int leng, Random rd = null)
        {
            string outPut = "";
            if (rd == null)
                rd = new Random();
            string validChars = "0123456789";
            for (int i = 0; i < leng; i++)
            {
                outPut += validChars[rd.Next(0, validChars.Length)];
            }
            return outPut;
        }

        /// <summary>
        /// Convert to Unsigned String
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string RunCMD(string cmd)
        {
            Process cmdProcess;
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.Arguments = "/c " + cmd;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.Start();
            string output = cmdProcess.StandardOutput.ReadToEnd();
            cmdProcess.WaitForExit();
            if (String.IsNullOrEmpty(output))
                return "";
            return output;
        }

        public static void DelayTime(double second)
        {
            Application.DoEvents();
            Thread.Sleep(Convert.ToInt32(second * 1000));
        }

        public static string HtmlDecode(string text)
        {
            return WebUtility.HtmlDecode(text);
        }
        public static string HtmlEncode(string text)
        {
            return WebUtility.HtmlEncode(text);
        }
        public static string UrlDecode(string text)
        {
            return WebUtility.UrlDecode(text);
        }
        public static string UrlEncode(string text)
        {
            return WebUtility.UrlEncode(text);
        }

        #region Chia Màn hình 3x2
        public static int getWidthScreen = Screen.PrimaryScreen.Bounds.Width;
        public static int getHeightScreen = Screen.PrimaryScreen.Bounds.Height;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">3x2; 4x2; 5x2; 6x2; 4x3</param>
        /// <returns></returns>
        public static Point GetSizeChrome(int column, int row)
        {
            int getWidthChrome = getWidthScreen / column + 15;
            int getHeightChrome = getHeightScreen / row + 10;
            return new Point(getWidthChrome, getHeightChrome);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">1-3x2; 2-4x2; 3-5x2; 4-6x2; 5-4x3</param>
        /// <returns></returns>
        public static Point GetPointFromIndexPosition(int indexPos, int column, int row)
        {
            Point location = new Point();
            if (indexPos >= column * row)
                indexPos -= column * row;

            switch (row)
            {
                case 1:
                    location.Y = 0;
                    break;
                case 2:
                    if (indexPos < column)
                    {
                        location.Y = 0;
                    }
                    else if (indexPos < column * 2)
                    {
                        int x = indexPos / column;
                        location.Y = getHeightScreen / 2;
                        indexPos -= column;
                    }
                    break;
                case 3:
                    if (indexPos < column)
                    {
                        location.Y = 0;
                    }
                    else if (indexPos < column * 2)
                    {
                        location.Y = getHeightScreen / 3 * 1;
                        indexPos -= column;
                    }
                    else if (indexPos < column * 3)
                    {
                        location.Y = getHeightScreen / 3 * 2;
                        indexPos -= column * 2;
                    }
                    break;
                case 4:
                    if (indexPos < column)
                    {
                        location.Y = 0;
                    }
                    else if (indexPos < column * 2)
                    {
                        location.Y = getHeightScreen / 4 * 1;
                        indexPos -= column;
                    }
                    else if (indexPos < column * 3)
                    {
                        location.Y = getHeightScreen / 4 * 2;
                        indexPos -= column * 2;
                    }
                    else if (indexPos < column * 4)
                    {
                        location.Y = getHeightScreen / 4 * 3;
                        indexPos -= column * 3;
                    }
                    break;
                case 5:
                    if (indexPos < column)
                    {
                        location.Y = 0;
                    }
                    else if (indexPos < column * 2)
                    {
                        location.Y = getHeightScreen / 5 * 1;
                        indexPos -= column;
                    }
                    else if (indexPos < column * 3)
                    {
                        location.Y = getHeightScreen / 5 * 2;
                        indexPos -= column * 2;
                    }
                    else if (indexPos < column * 4)
                    {
                        location.Y = getHeightScreen / 5 * 3;
                        indexPos -= column * 3;
                    }
                    else
                    {
                        location.Y = getHeightScreen / 5 * 4;
                        indexPos -= column * 4;
                    }
                    break;
                default:
                    break;
            }

            //location.Y = 0;
            //if (indexPos > 0)
            //{
            //    int x = column / indexPos;
            //    location.Y = getHeightScreen / x;
            //    indexPos -= column * (x-1);
            //}


            int widthWindowChrome = getWidthScreen / column;
            location.X = (indexPos) * (widthWindowChrome) - 10;
            return location;
        }
        public static int GetIndexOfPossitionApp(ref List<int> lstPossition)
        {
            int indexPos = 0;
            lock (lstPossition)
            {
                for (int i = 0; i < lstPossition.Count; i++)
                {
                    if (lstPossition[i] == 0)
                    {
                        indexPos = i;
                        lstPossition[i] = 1;
                        break;
                    }
                }
            }
            return indexPos;
        }
        public static void FillIndexPossition(ref List<int> lstPossition, int indexPos)
        {
            lock (lstPossition)
            {
                lstPossition[indexPos] = 0;
            }
        }
        #endregion

        static object k = new object();
        public static List<string> GetListValidFilePath(List<string> lstFilePath)
        {
            List<string> lst = new List<string>();
            try
            {
                string pathFile = "";
                for (int i = 0; i < lstFilePath.Count; i++)
                {
                    pathFile = lstFilePath[i];
                    if (File.Exists(pathFile))
                        lst.Add(pathFile);
                }
            }
            catch
            {
            }
            return lst;
        }
        public static string CheckProxy(string proxy, int typeProxy)
        {
            string ip = "";
            try
            {
                RequestXNet request = new RequestXNet("", "", proxy, typeProxy);
                ip = request.RequestGet("https://api6.ipify.org/"); 
            }
            catch (Exception ex)
            {
                MCommon.Common.ExportError(null, ex, "Check Proxy");
            }
            return ip;
        }
        public static string CheckIP()
        {
            string ip = "";

            try
            {
                RequestXNet request = new RequestXNet("", "", "", 0);
                string rq = "";
                rq=request.RequestGet("http://lumtest.com/myip.json");
                ip = JObject.Parse(rq)["ip"].ToString();
                //ip = request.RequestGet("http://icanhazip.com/").Trim();
            }
            catch
            {
            }
            return ip;
        }
        public static bool ChangeIP(int typeChangeIp, string profileDcom = "")
        {
            bool isSuccess = false;
            try
            {
                string ip_old = CheckIP();
                string ip_new = "";
                if (typeChangeIp == 0)
                {
                    //Ko đổi ip
                }
                else if (typeChangeIp == 2)
                {
                    IntPtr app = AutoControl.FindWindowHandle(null, "HMA VPN");
                    AutoControl.BringToFront(app);
                    AutoControl.SendClickOnPosition(AutoControl.FindHandle(app, "Chrome_RenderWidgetHostHWND", "Chrome Legacy Window"), 356, 286);
                    Thread.Sleep(5000);
                    string ipLan = CheckIP();
                    AutoControl.SendClickOnPosition(AutoControl.FindHandle(app, "Chrome_RenderWidgetHostHWND", "Chrome Legacy Window"), 356, 286);
                    //Thread.Sleep(15000);
                    int timeStart = Environment.TickCount;
                    do
                    {
                        ip_new = CheckIP();
                        if (Environment.TickCount - timeStart > 20000)
                            break;
                    } while (ip_new == ip_old || ip_new == ipLan);

                    if (ip_new != ip_old)
                        isSuccess = true;
                }
                else if (typeChangeIp == 1)
                {
                    ResetDcom(profileDcom);
                    ip_new = CheckIP();
                    if (ip_new != ip_old)
                        isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ExportError(null, ex);
            }
            return isSuccess;
        }

        /// <summary>
        /// Create 1 folder name "log" and create 2 folder name "images" and "html" into "log"
        /// </summary>
        public static void ExportError(Chrome chrome, Exception ex, string error = "")
        {
            try
            {
                if (!Directory.Exists("log"))
                    Directory.CreateDirectory("log");
                if (!Directory.Exists("log\\html"))
                    Directory.CreateDirectory("log\\html");
                if (!Directory.Exists("log\\images"))
                    Directory.CreateDirectory("log\\images");

                Random rrrd = new Random();
                string fileName = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + "_" + rrrd.Next(1000, 9999);

                if (chrome != null)
                {
                    string html = chrome.ExecuteScript("var markup = document.documentElement.innerHTML;return markup;").ToString();
                    chrome.ScreenCapture(@"log\images\", fileName);
                    File.WriteAllText(@"log\html\" + fileName + ".html", html);
                }

                using (StreamWriter writer = new StreamWriter(@"log\log.txt", true))
                {
                    writer.WriteLine("-----------------------------------------------------------------------------");
                    writer.WriteLine("Date: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    writer.WriteLine("File: " + fileName);
                    if (error != "")
                        writer.WriteLine("Error: " + error);
                    writer.WriteLine();

                    if (ex != null)
                    {
                        writer.WriteLine("Type: " + ex.GetType().FullName);
                        writer.WriteLine("Message: " + ex.Message);
                        writer.WriteLine("StackTrace: " + ex.StackTrace);
                        ex = ex.InnerException;
                    }
                }
            }
            catch { }
        }
        public static bool ChangeIP(int typeChangeIP, string profileDcom, int iTypeHotspot, string sLinkNord)
        {
            bool isSuccess = false;
            string ip_new = "";
            string ip_old = CheckIP();
            try
            {
                if (typeChangeIP == 0)
                {
                    return true;
                }
                else if (typeChangeIP == 1)
                {
                    IntPtr app = AutoControl.FindWindowHandle(null, "HMA VPN");
                    AutoControl.BringToFront(app);
                    AutoControl.SendClickOnPosition(AutoControl.FindHandle(app, "Chrome_RenderWidgetHostHWND", "Chrome Legacy Window"), 356, 286);
                    Thread.Sleep(5000);
                    string ipLan = CheckIP();
                    AutoControl.SendClickOnPosition(AutoControl.FindHandle(app, "Chrome_RenderWidgetHostHWND", "Chrome Legacy Window"), 356, 286);
                    //Thread.Sleep(15000);
                    int timeStart = Environment.TickCount;
                    do
                    {
                        ip_new = CheckIP();
                        if (Environment.TickCount - timeStart > 20000)
                            break;
                    } while (ip_new == ip_old || ip_new == ipLan);

                    if (ip_new != ip_old)
                        isSuccess = true;
                }
                else if (typeChangeIP == 2)
                {
                    ResetDcom(profileDcom);
                    ip_new = CheckIP();
                    if (ip_new != ip_old)
                        isSuccess = true;
                }
                else if (typeChangeIP == 4)
                {
                    if (Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//disconnect-express.PNG"))
                        Thread.Sleep(5000);
                    if (Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//connect-express.PNG"))
                        Thread.Sleep(10000);
                    else
                    {
                        Auto.ClickControlImageFind("ExpressVPN", "mainScreen-vpn.PNG", "images//connect-express.PNG");
                        Thread.Sleep(10000);
                    }
                    ip_new = CheckIP();
                    if (ip_new != ip_old)
                        isSuccess = true;
                }
                else if (typeChangeIP == 5)
                {
                    if (iTypeHotspot == 0)
                    {
                        //hostpot
                        int timeStart = Environment.TickCount;
                        if (Auto.ClickControlImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG"))
                            do
                            {
                                if (Environment.TickCount - timeStart > 20000)
                                    return false;
                                Thread.Sleep(1000);
                            } while (Auto.ImageFind("Hotspot Shield", "images//connect-hotspot.PNG") == false);
                        Auto.ClickControlImageFind("Hotspot Shield", "images//connect-hotspot.PNG");
                        timeStart = Environment.TickCount;
                        do
                        {
                            if (Environment.TickCount - timeStart > 20000)
                                return false;
                            Thread.Sleep(1000);
                        } while (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") == false);
                    }
                    else if (iTypeHotspot == 1)
                    {
                        Random rd = new Random();
                        //hostpot
                        int timeStart = Environment.TickCount;
                        string pointChange = Auto.ImageFind1("Hotspot Shield", "images//change-country-hotspot.PNG");
                        //WriteError(pointChange + "|" + DateTime.Now.ToString());
                        Point pointcc = new Point();
                        if (pointChange != "")
                            pointcc = new Point(Convert.ToInt32(pointChange.Split('|')[0]), Convert.ToInt32(pointChange.Split('|')[1]));
                        if (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") && Auto.ClickControlImageFind("Hotspot Shield", "images//change-country-hotspot.PNG", 0, 0, 20) && pointChange != null)
                        {
                            Thread.Sleep(2000);
                            KAutoHelper.AutoControl.MouseScroll(pointcc, rd.Next(-10, 10), true);
                            Thread.Sleep(1000);
                            KAutoHelper.AutoControl.MouseClick(pointcc.X, pointcc.Y);
                            timeStart = Environment.TickCount;
                            do
                            {
                                if (Environment.TickCount - timeStart > 20000)
                                    return false;
                                Thread.Sleep(1000);
                            } while (Auto.ImageFind("Hotspot Shield", "images//disconnect-hotspot.PNG") == false);
                        }
                    }
                    ip_new = CheckIP();
                    if (ip_new != ip_old)
                        isSuccess = true;
                }
                else if (typeChangeIP == 6)
                {
                    //nordvpn
                    Random rd = new Random();
                    string[] arrGroupName = new string[]
                    {
                    "Albania",
                    "Argentina",
                    "Australia",
                    "Austria",
                    "Belgium",
                    "Bosnia and Herzegovina",
                    "Brazil",
                    "Bulgaria",
                    "Canada",
                    "Chile",
                    "Costa Rica",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Georgia",
                    "Germany",
                    "Greece",
                    "Hong Kong",
                    "Hungary",
                    "Iceland",
                    "India",
                    "Indonesia",
                    "Ireland",
                    "Israel",
                    "Italy",
                    "Japan",
                    "Latvia",
                    "Luxembourg",
                    "Mexico",
                    "Moldova",
                    "Netherlands",
                    "New Zealand",
                    "North Macedonia",
                    "Norway",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Serbia",
                    "Singapore",
                    "Slovakia",
                    "Slovenia",
                    "South Africa",
                    "South Korea",
                    "Spain",
                    "Sweden",
                    "Switzedland",
                    "Taiwan",
                    "Thailand",
                    "Turkey",
                    "Ukraine",
                    "United Kingdom",
                    "United States",
                    "Vietnam"
                    };
                    RunCMD("\"" + sLinkNord + "\\NordVPN.exe\" -d");
                    Thread.Sleep(10000);
                    RunCMD("\"" + sLinkNord + "\\NordVPN.exe\" -c -g" + arrGroupName[rd.Next(0, arrGroupName.Length - 1)]);
                    int timeStart = Environment.TickCount;
                    Thread.Sleep(5000);
                    do
                    {
                        Thread.Sleep(1000);
                        if (Environment.TickCount - timeStart > 20000)
                            return false;
                    } while (Auto.ImageFind("", "images//disconnect-nord.PNG", "HwndWrapper[hsscp.exe;;34304629-92de-4140-a008-a2b4910674c9]") == false);

                    ip_new = CheckIP();
                    if (ip_new != ip_old)
                        isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ExportError(null, ex, "Error ChangeIP");
            }
            return isSuccess;
        }

        #region Get Totp
        public static string GetTotp(string input)
        {
            string otp = "";
            try
            {
                RequestXNet request = new RequestXNet("", "", "", 0);
                string html = "";
                string url = "http://app.minsoftware.vn/api/2fa?secret=" + input.Replace(" ", "");
                for (int i = 0; i < 5; i++)
                {
                    html = request.RequestGet(url);

                    if (MCommon.Common.IsNumber(html))
                    {
                        for (int j = html.Length; j < 6; j++)
                        {
                            html = "0" + html;
                        }
                        otp = html;
                        break;
                    }
                }
            }
            catch
            {
            }

            return otp;

            //byte[] key = ToBytes(input);

            //var window = CalculateTime30FromTimestamp(DateTime.UtcNow);

            //var data = GetBigEndianBytes(window);

            //var hmac = new HMACSHA1();
            //hmac.Key = key;
            //var hmacComputedHash = hmac.ComputeHash(data);

            //int offset = hmacComputedHash[hmacComputedHash.Length - 1] & 0x0F;
            //var otp = (hmacComputedHash[offset] & 0x7f) << 24
            //       | (hmacComputedHash[offset + 1] & 0xff) << 16
            //       | (hmacComputedHash[offset + 2] & 0xff) << 8
            //       | (hmacComputedHash[offset + 3] & 0xff) % 1000000;

            //var result = Digits(otp, 6);

            //return result;
        }

        private static int RemainingSeconds()
        {
            return 30 - (int)(((DateTime.UtcNow.Ticks - 621355968000000000L) / 10000000L) % 30);
        }

        private static byte[] GetBigEndianBytes(long input)
        {
            // Since .net uses little endian numbers, we need to reverse the byte order to get big endian.
            var data = BitConverter.GetBytes(input);
            Array.Reverse(data);
            return data;
        }

        private static long CalculateTime30FromTimestamp(DateTime timestamp)
        {
            var unixTimestamp = (timestamp.Ticks - 621355968000000000L) / 10000000L;
            var window = unixTimestamp / (long)30;
            return window;
        }

        private static string Digits(long input, int digitCount)
        {
            var truncatedValue = ((int)input % (int)Math.Pow(10, digitCount));
            return truncatedValue.ToString().PadLeft(digitCount, '0');
        }

        private static byte[] ToBytes(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException("input");
            }

            input = input.TrimEnd('='); //remove padding characters
            int byteCount = input.Length * 5 / 8; //this must be TRUNCATED
            byte[] returnArray = new byte[byteCount];

            byte curByte = 0, bitsRemaining = 8;
            int mask = 0, arrayIndex = 0;

            foreach (char c in input)
            {
                int cValue = CharToValue(c);

                if (bitsRemaining > 5)
                {
                    mask = cValue << (bitsRemaining - 5);
                    curByte = (byte)(curByte | mask);
                    bitsRemaining -= 5;
                }
                else
                {
                    mask = cValue >> (5 - bitsRemaining);
                    curByte = (byte)(curByte | mask);
                    returnArray[arrayIndex++] = curByte;
                    curByte = (byte)(cValue << (3 + bitsRemaining));
                    bitsRemaining += 3;
                }
            }

            //if we didn't end with a full byte
            if (arrayIndex != byteCount)
            {
                returnArray[arrayIndex] = curByte;
            }

            return returnArray;
        }

        private static int CharToValue(char c)
        {
            int value = (int)c;

            //65-90 == uppercase letters
            if (value < 91 && value > 64)
            {
                return value - 65;
            }
            //50-55 == numbers 2-7
            if (value < 56 && value > 49)
            {
                return value - 24;
            }
            //97-122 == lowercase letters
            if (value < 123 && value > 96)
            {
                return value - 97;
            }

            throw new ArgumentException("Character is not a Base32 character.", "c");
        }
        #endregion
    }
}
