﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using maxcare.Properties;

namespace MCommon
{
    class xproxyProxy
    {
        object k = new object();

        object k1 = new object();
        int typeProxy;

        public xproxyProxy(string ServicesURL, string proxy,int typeProxy, int limit_theads_use)
        {
            this.ServicesURL = ServicesURL;
            this.proxy = proxy;
            this.limit_theads_use = limit_theads_use;
            this.ip = "";
            this.typeProxy = typeProxy;
        }

        public void DecrementDangSuDung1()
        {
            lock (k)
            {
                dangSuDUng--;
                if (dangSuDUng == 0)
                {
                    daSuDung = 0;
                    canChangeIP = true;
                }
            }
        }

        public bool ResetProxy()
        {
            bool success = false;
            try
            {
                ServicesURL = ServicesURL.TrimEnd('/');
                string url = ServicesURL + "/reset?proxy=" + proxy;
                RequestXNet request = new RequestXNet("","","",0);
                string html = request.RequestGet(url);

                if (JObject.Parse(html)["msg"].ToString() == "command_sent")
                {
                    for (int i = 0; i < 120; i++)
                    {
                        if (CheckLiveProxy())
                        {
                            for(int j = 0; j < 20; j++)
                            {
                                string ipt = Common.CheckProxy(proxy, typeProxy);
                                if (ipt != "")
                                {
                                    this.ip = ipt;
                                    break;
                                }
                                else
                                {
                                    Thread.Sleep(1000);
                                }
                            }
                            return true;
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch
            {
                success = false;
            }
            return success;
        }

        public string tryToGetMyIP()
        {

            lock (k1)
            {
                if (canChangeIP)
                {
                    if (dangSuDUng > 1)
                    {
                        daSuDung--;
                        dangSuDUng--;
                        return "-2";
                    }
                    else
                    {
                        if (ResetProxy())
                        {
                            if (daSuDung > limit_theads_use)
                            {
                                daSuDung = 0;
                                canChangeIP = true;
                            }
                            else
                                canChangeIP = false;
                            return "1";
                        }
                        else
                        {
                            return "0";
                        }
                    }
                }
                else
                {
                    if (daSuDung <= limit_theads_use)
                    {
                        return "1";
                    }
                    else
                    {
                        daSuDung--;
                        canChangeIP = true;
                        return "-1";
                    }
                }
            }
        }

        public void DecrementDangSuDung()
        {
            lock (k)
            {
                dangSuDUng--;
                if (dangSuDUng == 0)
                {
                    daSuDung = 0;
                    canChangeIP = true;
                }
            }
        }

        public bool CheckLiveProxy()
        {
            bool isSuccess = false;
            try
            {
                ServicesURL = ServicesURL.TrimEnd('/');
                string url = ServicesURL + "/status?proxy=" + proxy;
                RequestXNet request = new RequestXNet("","","",0);
                string html = request.RequestGet(url);
                isSuccess = Convert.ToBoolean(JObject.Parse(html)["status"].ToString());
            }
            catch
            {
            }
            return isSuccess;
        }

        private string ServicesURL;

        public string proxy;

        private bool isStop = false;

        public string ip = "";

        //đang đổi ip
        public bool isChangingIp = false;

        //có thể đổi
        public bool canChangeIP = true;

        //số lượng đang sử dụng
        public int dangSuDUng = 0;

        //đã sử dụng
        public int daSuDung = 0;

        //số lượng tối đa cùng lúc
        public int limit_theads_use = 3;
    }
}
