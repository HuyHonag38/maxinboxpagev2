﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MCommon
{
    class CommonRequest
    {
        public static bool CheckLiveCookie(string cookie)
        {
            string uid = Regex.Match(cookie + ";", "c_user=(.*?);").Groups[1].Value;
            try
            {
                RequestXNet request = new RequestXNet(cookie,"","",0);
                if (uid != "")
                {
                    string html = request.RequestGet("https://www.facebook.com/me").ToString();
                    if (html.Contains("id=\"code_in_cliff\"") || html.Contains("name=\"new\"") || html.Contains("name=\"c\""))
                        return true;
                    else if (Regex.Match(html, "\"USER_ID\":\"(.*?)\"").Groups[1].Value.Trim() == uid.Trim() && html.Contains("checkpointSubmitButton") == false && html.Contains("checkpointBottomBar") == false && html.Contains("captcha_response") == false)
                        return true;
                }
            }
            catch
            { }
            return false;
        }

        #region Check avatar

        #endregion

        public static bool CheckLiveToken(string token, string useragent, string proxy, int typeProxy)
        {
            bool isLive = false;
            RequestXNet request = new RequestXNet("", useragent, proxy, typeProxy);

            try
            {
                string infor = request.RequestGet("https://graph.facebook.com/me?access_token=" + token);
                isLive = true;
            }
            catch
            {
            }

            return isLive;
        }
        public static string GetTokenEAAAAZ(string cookie)
        {
            string token = "";
            try
            {
                RequestXNet request = new RequestXNet(cookie, "", "", 0);
                string rq = request.RequestGet("https://m.facebook.com/composer/ocelot/async_loader/?publisher=feed");
                token = Regex.Match(rq, "EAAAAZ(.*?)\"").Value.Replace("\"", "").Replace(@"\", "");
            }
            catch
            {
            }

            if (token == "" && !CheckLiveCookie(cookie))
                return "-1";

            return token;
        }


        public static string CheckCheckpoint(string idMethod)
        {
            string stt = "";
            int typeLag = 0;
            switch (idMethod)
            {
                case "3":
                    if (typeLag == 0)
                        stt = "Ảnh";
                    else
                        stt = "Image";
                    break;
                case "2":
                    if (typeLag == 0)
                        stt = "Ngày sinh";
                    else
                        stt = "Birthday";
                    break;
                case "20":
                    if (typeLag == 0)
                        stt = "Tin nhắn";
                    else
                        stt = "Message";
                    break;
                case "4":
                case "34":
                    stt = "Otp";
                    break;
                case "37":
                    stt = "Gửi OTP về mail";
                    break;
                case "35":
                    stt = "Login Google";
                    break;
                case "14":
                    if (typeLag == 0)
                        stt = "Thiết bị";
                    else
                        stt = "device";
                    break;
                case "26":
                    if (typeLag == 0)
                        stt = "Nhờ bạn bè";
                    else
                        stt = "Friend";
                    break;
                case "18":
                    if (typeLag == 0)
                        stt = "Bình luận";
                    else
                        stt = "comment";
                    break;
                case "72h":
                    if (typeLag == 0)
                        stt = "72h";
                    else
                        stt = "72 hours";
                    break;
                case "vhh":
                    if (typeLag == 0)
                        stt = "Vô hiệu hóa";
                    else
                        stt = "disable";
                    break;
                case "id_upload":
                    stt = "Up ảnh";
                    break;
                case "2fa":
                    stt = "Có 2fa";
                    break;
                default:
                    File.AppendAllText(@"data\dangcp.txt", idMethod);
                    break;
            }
            return stt;
        }

        /// <summary>
        /// 0-Không xác định, 1-live, 2-checkpoint, 3-sai pass, 4-sai email, 5-có 2fa
        /// </summary>
        /// <param name="email"></param>
        /// <param name="pass"></param>
        /// <returns>0-Không xác định, 1-live, 2-checkpoint, 3-sai pass, 4-sai email, 5-có 2fa</returns>

    }
}
