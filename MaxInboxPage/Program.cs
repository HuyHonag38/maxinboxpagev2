﻿using Common;
using DeviceId;
using License.RNCryptor;
using maxcare.Properties;
using MCommon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxcare
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] input)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Khi nào xuất tool thì phải comment 2 dòng này lại
            try
            {
                Application.Run(new fMain(""));
            }
            catch (Exception ex)
            {
                MCommon.Common.ShowMessageBox("Có lỗi xảy ra, vui lòng liên hệ Admin để được hỗ trợ!", 2);
                MCommon.Common.ExportError(null, ex, "Run Program");
            }
            return;

            bool isCalledByOtherProgram = false;
            string userName = ""; string pass = "";
            RequestXNet request = new RequestXNet("", "", "", 0);
            Decryptor decrypt = new Decryptor();
            Random rd = new Random();
            string checkLisence = "";
            string deviceId = "";
            string api_token = "";
            int codekeyrandom = rd.Next(0, 10000) + rd.Next(100, 1000);
            deviceId = CommonCSharp.Md5Encode(new DeviceIdBuilder().AddMachineName().AddProcessorId().AddMotherboardSerialNumber().AddSystemDriveSerialNumber().ToString());

            if (input.Length > 0)
            {
                checkLisence = input[0];
                if (input.Length >= 2)
                    Base.profilePath = input[1];
                isCalledByOtherProgram = true;
                Base.isCalledByOtherProgram = true;
            }
            else
            {
                userName = Settings.Default.UserName;
                pass = Settings.Default.PassWord;

                if (userName == "" || pass == "" || !CommonCSharp.IsValidMail(userName))
                {
                    OpenfActive(deviceId);
                    return;
                }

                int iRandom = rd.Next(0, 999999);
                api_token = CommonCSharp.ReadHTMLCode("http://app.minsoftware.vn/api/auth?datavery=" + CommonCSharp.Base64Encode(userName + "|" + pass)).Replace("\"", "");
                if (api_token.Trim() == "")
                {
                    OpenfActive(deviceId);
                    return;
                }

                string url = "http://app.minsoftware.vn/minapi/minapi/api.php/Check?data=";
                string strRequest = deviceId + "|" + api_token + "|" + fMain.softIndex + "|" + codekeyrandom + "|" + "minsoftware0803";
                Encryptor encrypt = new Encryptor();
                string strEncryptRequest = encrypt.Encrypt(strRequest, "thangtungmin080394");
                checkLisence = request.RequestGet(url + strEncryptRequest).Replace("\"", "");
            }

            checkLisence = CommonCSharp.Base64Decode(checkLisence);
            checkLisence = decrypt.Decrypt(checkLisence, "thangtungmin080394");
            if (checkLisence == null || checkLisence == "null")
            {
                MessageBox.Show("Lỗi hệ thống!!!");
                return;
            }
            if (checkLisence.Contains("chuakichhoat"))
            {
                OpenfActive(deviceId);
                return;
            }
            if (checkLisence.Contains("error"))
            {
                OpenfActive(deviceId);
                return;
            }
            if (checkLisence.Contains("hethan"))
            {
                OpenfActive(deviceId);
                return;
            }

            string full_name = checkLisence.Split('|')[0];
            string api_token_sv = checkLisence.Split('|')[1];
            string date_exp = checkLisence.Split('|')[2];
            string device_server = checkLisence.Split('|')[3];
            string codekeyrandom_sv = checkLisence.Split('|')[4];
            string keystatic_sv = checkLisence.Split('|')[5];

            if (isCalledByOtherProgram)
            {
                codekeyrandom_sv = codekeyrandom.ToString();
                api_token = api_token_sv;
            }


            if (deviceId != device_server || api_token_sv != api_token || codekeyrandom_sv != codekeyrandom.ToString() || keystatic_sv != "minsoftware0803")
            {
                OpenfActive(deviceId);
                return;
            }
            else
            {
                try
                {
                    Application.Run(new fMain(checkLisence));
                }
                catch (Exception ex)
                {
                    MCommon.Common.ShowMessageBox("Có lỗi xảy ra, vui lòng liên hệ Admin để được hỗ trợ!", 2);
                    MCommon.Common.ExportError(null, ex, "Run Program");
                }
            }
        }

        static void OpenfActive(string deviceId)
        {
            if (deviceId == "")
                deviceId = CommonCSharp.Md5Encode(new DeviceIdBuilder().AddMachineName().AddProcessorId().AddMotherboardSerialNumber().AddSystemDriveSerialNumber().ToString());
            Application.Run(new fActive(1, deviceId));
        }
    }
}
